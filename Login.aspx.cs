﻿using BigData.AppCode;
using System;
using System.Data.SqlClient;

namespace BigData
{
    public partial class Login : System.Web.UI.Page
    {
        //登入頁面載入
        protected void Page_Load(object sender, EventArgs e)
        {
            BigData_DB_Function Tool = new BigData_DB_Function();

            if (System.Web.HttpContext.Current.Request.Url.AbsoluteUri.Contains("56387"))
            {
                // 設定用戶狀態為登入
                SetLoginSession();
                Session["WorkStation"] = "UUT_TEST";
                // 登入後，轉到"BEC診斷"頁面
                Response.Redirect("WebForm4.aspx");
            }

            //讀取url是否有ID參數
            if (!string.IsNullOrEmpty(Request.QueryString["ID"]) && !string.IsNullOrEmpty(Request.QueryString["WorkStation"]))
            {
                string sUserAccount = Request.QueryString["ID"].ToString();
                string sWorkStation = Request.QueryString["WorkStation"].ToString();
                // 取得用戶名
                //if (GetUserName(sUserAccount))
                if (Tool.IsUserExist(sUserAccount))
                {
                    // 設定用戶狀態為登入
                    SetLoginSession();
                    Session["WorkStation"] = sWorkStation;
                    // 登入後，轉到"BEC診斷"頁面
                    Response.Redirect("WebForm4.aspx");
                }
                else
                {
                    Response.Redirect("Info.aspx?info=1");
                    //Response.Write("<script language=JavaScript> alert('用戶不存在，導向系統登入頁面!'); window.location='Login.aspx'; </script>");
                }
            }
            else if (string.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                Response.Redirect("Info.aspx?info=1");
            }
            else if (string.IsNullOrEmpty(Request.QueryString["WorkStation"]))
            {
                Response.Redirect("Info.aspx?info=2");
            }
        }

        // 按下登入按鈕時，做帳戶存在與否及密碼檢查
        protected void Login_Click(object sender, EventArgs e)
        {
            string sUserAccount = Account.Text.ToString();
            string sUserPwd = Password.Text.ToString();

            // 宣告validAccount確認此用戶是否存在，密碼是否正確
            bool validAccount = CheckUserExist(sUserAccount, sUserPwd);
            // 若用戶存在
            if (validAccount)
            {
                // 取得用戶名
                GetUserName(sUserAccount);
                // 設定用戶狀態為登入
                SetLoginSession();
                // 登入後，轉到"BEC診斷"頁面
                Response.Redirect("WebForm4.aspx");
            }// 若用戶不存在，或密碼輸入錯誤
            else
            {
                // 跳出"請輸入正確的帳號密碼!"提醒視窗
                Response.Write("<script language=JavaScript> alert('請輸入正確的帳號密碼!'); </script>");
            }
        }

        private void SetLoginSession()
        {
            // 設定登入狀態為true
            Session["isLogin"] = true;
        }

        protected void SetAccountID(object sender, EventArgs e)
        {
            // 存取用戶帳號
            AccountID.Text = HiddenAccount.Value;
        }

        protected void SetOriginPWD(object sender, EventArgs e)
        {
            // 存取用戶密碼
            OriginPWD.Text = HiddenOriginPWD.Value;
        }

        protected void SetNewPWD(object sender, EventArgs e)
        {
            // 存取用戶新密碼
            NewPWD.Text = HiddenNewPWD.Value;
        }

        protected void SetConfirmNewPWD(object sender, EventArgs e)
        {
            // 存取用戶確認新密碼(更換密碼時輸入的第二次密碼)
            ConfirmNewPWD.Text = HiddenConfirmNewPWD.Value;
        }

        // 當按下更換密碼時，做帳戶檢查，並更新密碼
        protected void ChangePWD_Click(object sender, EventArgs e)
        {
            // 用戶輸入帳號
            string account = HiddenAccount.Value;
            // 用戶輸入原密碼
            string orgPassword = HiddenOriginPWD.Value;
            // 宣告validAccount確認此用戶是否存在，密碼是否正確
            bool validAccount = CheckUserExist(account, orgPassword);
            // 若用戶存在
            if (validAccount)
            {
                // 用戶輸入的新密碼
                string newPWD = NewPWD.Text.ToString();
                // 用戶輸入的第二次新密碼
                string confirmNewPWD = ConfirmNewPWD.Text.ToString();
                // 確認用戶輸入的新密碼與第二次輸入的新密碼相同
                bool isSame = newPWD.Equals(confirmNewPWD);
                // 若兩密碼相同
                if (isSame)
                {
                    // 確認用戶輸入的密碼格式正確
                    bool isValidPWD = CheckPWDValid(newPWD);
                    // 若密碼格是正確
                    if (isValidPWD)
                    {
                        // 更新用戶密碼
                        UpdateUserPassword(account, newPWD);
                    }
                }// 若輸入的兩個密碼不同
                else
                {
                    // 跳出"請檢查新密碼與確認新密碼相同!"提醒視窗
                    Response.Write("<script language=JavaScript> alert('請檢查新密碼與確認新密碼相同!'); </script>");
                }
            }// 若檢查用戶不存在或密碼輸入錯誤
            else
            {
                // 跳出"請輸入正確的帳號密碼!"提醒視窗
                Response.Write("<script language=JavaScript> alert('請輸入正確的帳號密碼!'); </script>");
            }
        }

        // 檢查密碼格式
        private bool CheckPWDValid(string newPWD)
        {
            // 檢查密碼是否為六位數字
            bool validPassword = int.TryParse(newPWD, out int n) && newPWD.Length == 6;
            // 若密碼格式錯誤
            if (!validPassword)
            {
                // 跳出"密碼必須為六位數字!"提醒視窗
                Response.Write("<script language=JavaScript> alert('密碼必須為六位數字!'); </script>");
            }
            // 回傳密碼格式是否正確
            return validPassword;
        }

        // 更新用戶密碼
        private void UpdateUserPassword(string account, string password)
        {
            // 連接資料庫
            SqlConnection connection = OpenSQLConnection();
            // SQL更新密碼語法
            string command = "UPDATE [dbo].[UserInform] SET[Password] = '" + password + "' WHERE Account = '" + account + "'";
            // 新增SQL命令
            SqlCommand cmd = new SqlCommand(command, connection);
            // 執行SQL命令
            cmd.ExecuteNonQuery();
            // 關閉資料庫連線
            connection.Close();
            // 跳出"更新成功!"提醒視窗
            Response.Write("<script language=JavaScript> alert('更新成功!'); </script>");
        }

        /// <summary>
        /// 存取用戶名稱
        /// </summary>
        /// <param name="Account">用戶帳號</param>
        /// <returns>true=用戶存在，反之表示用戶不存在</returns>
        private bool GetUserName(string Account)
        {
            bool bIsUserExist = false;
            // 連接資料庫
            SqlConnection connection = OpenSQLConnection();
            // SQL拿取用戶名稱語法
            var command = "SELECT Username FROM UserInform where Account = @Account";
            // 新增SQL命令
            SqlCommand cmd = new SqlCommand(command, connection);
            cmd.Parameters.AddWithValue("@Account", Account);
            // SQL 讀取資料
            SqlDataReader reader = cmd.ExecuteReader();
            // 讀取所有資料
            while (reader.Read())
            {
                // 設定用戶名稱
                Session["Username"] = reader.GetString(0);
                bIsUserExist = true;
            }
            // 關閉資料庫連線
            connection.Close();

            return bIsUserExist;
        }

        // 確認用戶是否存在
        private bool CheckUserExist(string account, string password)
        {
            // 連接資料庫
            SqlConnection connection = OpenSQLConnection();
            // SQL讀取帳號密碼相符的用戶
            var command = "SELECT * FROM UserInform where Account = @Account and Password = @Password";
            // 新增SQL命令
            SqlCommand cmd = new SqlCommand(command, connection);
            cmd.Parameters.AddWithValue("@Account", account);
            cmd.Parameters.AddWithValue("@Password", password);
            // SQL 讀取資料
            SqlDataReader reader = cmd.ExecuteReader();
            // 設定一開始不存在
            bool IsUserExist = false;
            // 讀取是否有資料
            if (reader.Read())
            {
                IsUserExist = true;
            }
            // 關閉資料庫連線
            connection.Close();
            // 回傳用戶存在狀態
            return IsUserExist;
        }

        // 連接資料庫
        private static SqlConnection OpenSQLConnection()
        {
            // 設定要連線的資料庫及登入的帳密
            String conn = "Server=localhost;User id=LoginAIDC;Password=aidc2019;database=test;";
            // 建立連線
            SqlConnection connection = new SqlConnection(conn);
            // 開啟連線
            connection.Open();
            // 回傳該連線
            return connection;
        }
    }
}