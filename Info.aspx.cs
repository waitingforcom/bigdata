﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BigData
{
    public partial class Info : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //讀取url是否有ID參數
                if (!string.IsNullOrEmpty(Request.QueryString["info"]))
                {
                    string sInfo = Request.QueryString["info"].ToString();
                    switch (sInfo)
                    {
                        case "1":
                            lbl_Info.Text = "使用者不存在";
                            break;
                        case "2":
                            lbl_Info.Text = "測台參數錯誤";
                            break;
                        case "3":
                            lbl_Info.Text = "閒置太久，請重新進入";
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }
}