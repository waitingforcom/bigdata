﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="BigData.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title></title>
    <style type="text/css">
        .auto-style1 {
            text-align: center;
            font-size: x-large;
            font-family: DFKai-sb,"標楷體";
        }

        .auto-style3 {
            text-align: left;
            font-size: x-large;
            font-family: DFKai-sb,"標楷體";
        }

        .auto-style4 {
            font-size: xx-large;
            font-family: DFKai-sb,"標楷體";
        }

        .GridViewEditRow input[type=text] {
            width: 100px;
        }
    </style>
    <link href="layout.css" rel="stylesheet" type="text/css" />
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <script type="text/javascript" src="main.js"></script>
</head>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script>
    $(function () {
        $("#Confirm").click(function () {
            var startYear = $("#<%=StartYear.ClientID%>").val();
            var endYear = $("#<%=EndYear.ClientID%>").val();
            var nowYear = new Date().getFullYear();
            if (!(isANumber(startYear)) || !(isANumber(endYear))) {
                alert("請輸入正確資料格式");
                $("#<%=StartYear.ClientID%>").val("");
                $("#<%=EndYear.ClientID%>").val("");
            }
            else if (startYear > endYear || nowYear + 1 < endYear) {
                alert("請檢查年份範圍");
                $("#<%=StartYear.ClientID%>").val("");
                $("#<%=EndYear.ClientID%>").val("");
            }
            else {
                $("#ConfirmBtn").click();
            }
        })
    })

    function isANumber(str) {
        return !/\D/.test(str);
    }
</script>
<body>
    <form id="form1" runat="server">
        <div class="container-fluid img-fluid">
            <asp:Image ID="img_Title" runat="server" ImageUrl="~/title1.png" />
            <p hidden>
                <asp:Label ID="PWD" runat="server"></asp:Label>
            </p>
        </div>

        <div id="NAV" class="container-fluid">
            <ul class="navbar-nav nav-fill w-100 d-inline-block">
                <li class="w-25"><a href="WebForm4.aspx">零件診斷</a></li>
                <li class="w-25" onclick="toggle()">
                    <a>新增維修紀錄 <span class="caret"></span></a>
                    <ul id="Dropdownlist" style="display: none;">
                        <li><a href="WebForm2.aspx">新增單筆維修紀錄</a></li>
                        <li><a href="WebForm3.aspx">新增多筆維修紀錄</a></li>
                    </ul>
                </li>
                <li class="w-25"><a href="WebForm9.aspx">資料庫功能</a></li>
                <%--<li><a class="active" href="WebForm1.aspx"><font size="6">歷史分析</font></a></li>--%>
                <li class="w-25"><a onclick="inputPassword()">管理頁面</a></li>
            </ul>
        </div>

        <p></p>
        <p></p>
        <br />
        <p></p>
        <div align="center" class="auto-style4">
            <font size="6px" style="color: aliceblue">歷史分析: 從</font>
            <asp:TextBox ID="StartYear" runat="server" Width="100px"></asp:TextBox>
            <font size="6px" style="color: aliceblue">年 ~ </font>
            <asp:TextBox ID="EndYear" runat="server" Width="100px"></asp:TextBox>
            <font size="6px" style="color: aliceblue">年</font>
            <button id="Confirm" type="button">確認</button>

        </div>
        <asp:Button ID="ConfirmBtn" runat="server" Text="確認" OnClick="ConfirmBtn_Click" Style="display: none;" />
        <p></p>


        <div id="CONTENT" class="auto-style3" style="text-align: center; background-color: transparent">
            <p style="color: aliceblue">BEC 零件失效機率圖</p>
            <asp:Chart ID="BEC_Chart" runat="server" BackColor="224, 224, 224" BackGradientStyle="Center" BorderlineDashStyle="Solid" DataSourceID="SqlDataSource1" Height="797px" IsMapEnabled="False" Width="1228px" BackImageAlignment="Center">
                <Series>
                    <asp:Series Color="DeepPink" IsXValueIndexed="True" Legend="Legend1" Name="Series1" XValueMember="top1_year" YValueMembers="top1_Failure_percent" BorderWidth="3">
                    </asp:Series>
                    <asp:Series ChartArea="ChartArea1" Color="Yellow" Legend="Legend1" Name="Series2" XValueMember="top2_year" YValueMembers="top2_Failure_percent" BorderWidth="3">
                    </asp:Series>
                    <asp:Series ChartArea="ChartArea1" Color="Red" Legend="Legend1" Name="Series3" XValueMember="top3_year" YValueMembers="top3_Failure_percent" BorderWidth="3">
                    </asp:Series>
                    <asp:Series ChartArea="ChartArea1" Color="Blue" Legend="Legend1" Name="Series4" XValueMember="top4_year" YValueMembers="top4_Failure_percent" BorderWidth="3">
                    </asp:Series>
                    <asp:Series ChartArea="ChartArea1" Color="Green" Legend="Legend1" Name="Series5" XValueMember="top5_year" YValueMembers="top5_Failure_percent" BorderWidth="3">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1" AlignmentOrientation="All">
                        <AxisY Maximum="60" Minimum="0" Title="失效機率%" TitleFont="標楷體, 24pt" TextOrientation="Stacked">
                        </AxisY>
                        <AxisX LineColor="128, 128, 255" Minimum="0" Title="年份" TitleFont="標楷體, 24pt">
                        </AxisX>
                    </asp:ChartArea>
                </ChartAreas>
                <Legends>
                    <asp:Legend ForeColor="ActiveCaptionText" HeaderSeparator="Line" InterlacedRows="True" IsEquallySpacedItems="True" Name="Legend1" BackImageAlignment="Left" Font="Consolas, 16.2pt, style=Bold" IsTextAutoFit="False">
                    </asp:Legend>
                </Legends>
                <Titles>
                    <asp:Title Alignment="TopCenter" DockedToChartArea="ChartArea1" Name="零件失效機率" BackImageAlignment="Top" Font="標楷體, 36pt" IsDockedInsideChartArea="False" Text="BEC 零件失效機率">
                    </asp:Title>
                </Titles>
            </asp:Chart>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [BEC]"></asp:SqlDataSource>
            <p></p>
            <p style="color: aliceblue">A1 零件失效機率圖</p>
            <asp:Chart ID="A1_Chart" runat="server" BackColor="224, 224, 224" BackGradientStyle="Center" BorderlineDashStyle="Solid" DataSourceID="SqlDataSource2" Height="797px" IsMapEnabled="False" Width="1228px" BackImageAlignment="Center">
                <Series>
                    <asp:Series Color="DeepPink" IsXValueIndexed="True" Legend="Legend1" Name="Series1" XValueMember="top1_year" YValueMembers="top1_Failure_percent" BorderWidth="3">
                    </asp:Series>
                    <asp:Series ChartArea="ChartArea1" Color="Yellow" Legend="Legend1" Name="Series2" XValueMember="top2_year" YValueMembers="top2_Failure_percent" BorderWidth="3">
                    </asp:Series>
                    <asp:Series ChartArea="ChartArea1" Color="Red" Legend="Legend1" Name="Series3" XValueMember="top3_year" YValueMembers="top3_Failure_percent" BorderWidth="3">
                    </asp:Series>
                    <asp:Series ChartArea="ChartArea1" Color="Blue" Legend="Legend1" Name="Series4" XValueMember="top4_year" YValueMembers="top4_Failure_percent" BorderWidth="3">
                    </asp:Series>
                    <asp:Series ChartArea="ChartArea1" Color="Green" Legend="Legend1" Name="Series5" XValueMember="top5_year" YValueMembers="top5_Failure_percent" BorderWidth="3">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1" AlignmentOrientation="All">
                        <AxisY Maximum="20" Minimum="0" Title="失效機率%" TitleFont="標楷體, 24pt" TextOrientation="Stacked">
                        </AxisY>
                        <AxisX LineColor="128, 128, 255" Minimum="0" Title="年份" TitleFont="標楷體, 24pt">
                        </AxisX>
                    </asp:ChartArea>
                </ChartAreas>
                <Legends>
                    <asp:Legend ForeColor="ActiveCaptionText" HeaderSeparator="Line" InterlacedRows="True" IsEquallySpacedItems="True" Name="Legend1" BackImageAlignment="Left" Font="Consolas, 16.2pt, style=Bold" IsTextAutoFit="False">
                    </asp:Legend>
                </Legends>
                <Titles>
                    <asp:Title Alignment="TopCenter" DockedToChartArea="ChartArea1" Name="零件失效機率" BackImageAlignment="Top" Font="標楷體, 36pt" IsDockedInsideChartArea="False" Text="A1 零件失效機率">
                    </asp:Title>
                </Titles>
            </asp:Chart>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [A1]"></asp:SqlDataSource>
            <p></p>
            <p style="color: aliceblue">A2 零件失效機率圖</p>
            <asp:Chart ID="A2_Chart" runat="server" BackColor="224, 224, 224" BackGradientStyle="Center" BorderlineDashStyle="Solid" DataSourceID="SqlDataSource3" Height="797px" IsMapEnabled="False" Width="1228px" BackImageAlignment="Center">
                <Series>
                    <asp:Series Color="DeepPink" IsXValueIndexed="True" Legend="Legend1" Name="Series1" XValueMember="top1_year" YValueMembers="top1_Failure_percent" BorderWidth="3">
                    </asp:Series>
                    <asp:Series ChartArea="ChartArea1" Color="Yellow" Legend="Legend1" Name="Series2" XValueMember="top2_year" YValueMembers="top2_Failure_percent" BorderWidth="3">
                    </asp:Series>
                    <asp:Series ChartArea="ChartArea1" Color="Red" Legend="Legend1" Name="Series3" XValueMember="top3_year" YValueMembers="top3_Failure_percent" BorderWidth="3">
                    </asp:Series>
                    <asp:Series ChartArea="ChartArea1" Color="Blue" Legend="Legend1" Name="Series4" XValueMember="top4_year" YValueMembers="top4_Failure_percent" BorderWidth="3">
                    </asp:Series>
                    <asp:Series ChartArea="ChartArea1" Color="Green" Legend="Legend1" Name="Series5" XValueMember="top5_year" YValueMembers="top5_Failure_percent" BorderWidth="3">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1" AlignmentOrientation="All">
                        <AxisY Maximum="70" Minimum="0" Title="失效機率%" TitleFont="標楷體, 24pt" TextOrientation="Stacked">
                        </AxisY>
                        <AxisX LineColor="128, 128, 255" Minimum="0" Title="年份" TitleFont="標楷體, 24pt">
                        </AxisX>
                    </asp:ChartArea>
                </ChartAreas>
                <Legends>
                    <asp:Legend ForeColor="ActiveCaptionText" HeaderSeparator="Line" InterlacedRows="True" IsEquallySpacedItems="True" Name="Legend1" BackImageAlignment="Left" Font="Consolas, 16.2pt, style=Bold" IsTextAutoFit="False">
                    </asp:Legend>
                </Legends>
                <Titles>
                    <asp:Title Alignment="TopCenter" DockedToChartArea="ChartArea1" Name="零件失效機率" BackImageAlignment="Top" Font="標楷體, 36pt" IsDockedInsideChartArea="False" Text="A2 零件失效機率">
                    </asp:Title>
                </Titles>
            </asp:Chart>
            <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [A2]"></asp:SqlDataSource>
            <p></p>
            <p style="color: aliceblue">A3 零件失效機率圖</p>
            <asp:Chart ID="A3_Chart" runat="server" BackColor="224, 224, 224" BackGradientStyle="Center" BorderlineDashStyle="Solid" DataSourceID="SqlDataSource4" Height="797px" IsMapEnabled="False" Width="1228px" BackImageAlignment="Center">
                <Series>
                    <asp:Series Color="DeepPink" IsXValueIndexed="True" Legend="Legend1" Name="Series1" XValueMember="top1_year" YValueMembers="top1_Failure_percent" BorderWidth="3">
                    </asp:Series>
                    <asp:Series ChartArea="ChartArea1" Color="Yellow" Legend="Legend1" Name="Series2" XValueMember="top2_year" YValueMembers="top2_Failure_percent" BorderWidth="3">
                    </asp:Series>
                    <asp:Series ChartArea="ChartArea1" Color="Red" Legend="Legend1" Name="Series3" XValueMember="top3_year" YValueMembers="top3_Failure_percent" BorderWidth="3">
                    </asp:Series>
                    <asp:Series ChartArea="ChartArea1" Color="Blue" Legend="Legend1" Name="Series4" XValueMember="top4_year" YValueMembers="top4_Failure_percent" BorderWidth="3">
                    </asp:Series>
                    <asp:Series ChartArea="ChartArea1" Color="Green" Legend="Legend1" Name="Series5" XValueMember="top5_year" YValueMembers="top5_Failure_percent" BorderWidth="3">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1" AlignmentOrientation="All">
                        <AxisY Maximum="20" Minimum="0" Title="失效機率%" TitleFont="標楷體, 24pt" TextOrientation="Stacked">
                        </AxisY>
                        <AxisX LineColor="128, 128, 255" Minimum="0" Title="年份" TitleFont="標楷體, 24pt">
                        </AxisX>
                    </asp:ChartArea>
                </ChartAreas>
                <Legends>
                    <asp:Legend ForeColor="ActiveCaptionText" HeaderSeparator="Line" InterlacedRows="True" IsEquallySpacedItems="True" Name="Legend1" BackImageAlignment="Left" Font="Consolas, 16.2pt, style=Bold" IsTextAutoFit="False">
                    </asp:Legend>
                </Legends>
                <Titles>
                    <asp:Title Alignment="TopCenter" DockedToChartArea="ChartArea1" Name="零件失效機率" BackImageAlignment="Top" Font="標楷體, 36pt" IsDockedInsideChartArea="False" Text="A3 零件失效機率">
                    </asp:Title>
                </Titles>
            </asp:Chart>
            <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [A3]"></asp:SqlDataSource>
            <p></p>
            <p style="color: aliceblue">Transducer 零件失效機率圖</p>
            <asp:Chart ID="Transducer_Chart" runat="server" BackColor="224, 224, 224" BackGradientStyle="Center" BorderlineDashStyle="Solid" DataSourceID="SqlDataSource5" Height="797px" IsMapEnabled="False" Width="1228px" BackImageAlignment="Center">
                <Series>
                    <asp:Series Color="DeepPink" IsXValueIndexed="True" Legend="Legend1" Name="Series1" XValueMember="top1_year" YValueMembers="top1_Failure_percent" BorderWidth="3">
                    </asp:Series>
                    <asp:Series ChartArea="ChartArea1" Color="Yellow" Legend="Legend1" Name="Series2" XValueMember="top2_year" YValueMembers="top2_Failure_percent" BorderWidth="3">
                    </asp:Series>
                    <asp:Series ChartArea="ChartArea1" Color="Red" Legend="Legend1" Name="Series3" XValueMember="top3_year" YValueMembers="top3_Failure_percent" BorderWidth="3">
                    </asp:Series>
                    <asp:Series ChartArea="ChartArea1" Color="Blue" Legend="Legend1" Name="Series4" XValueMember="top4_year" YValueMembers="top4_Failure_percent" BorderWidth="3">
                    </asp:Series>
                    <asp:Series ChartArea="ChartArea1" Color="Green" Legend="Legend1" Name="Series5" XValueMember="top5_year" YValueMembers="top5_Failure_percent" BorderWidth="3">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1" AlignmentOrientation="All">
                        <AxisY Maximum="80" Minimum="0" Title="失效機率%" TitleFont="標楷體, 24pt" TextOrientation="Stacked">
                        </AxisY>
                        <AxisX LineColor="128, 128, 255" Minimum="0" Title="年份" TitleFont="標楷體, 24pt">
                        </AxisX>
                    </asp:ChartArea>
                </ChartAreas>
                <Legends>
                    <asp:Legend ForeColor="ActiveCaptionText" HeaderSeparator="Line" InterlacedRows="True" IsEquallySpacedItems="True" Name="Legend1" BackImageAlignment="Left" Font="Consolas, 16.2pt, style=Bold" IsTextAutoFit="False">
                    </asp:Legend>
                </Legends>
                <Titles>
                    <asp:Title Alignment="TopCenter" DockedToChartArea="ChartArea1" Name="零件失效機率" BackImageAlignment="Top" Font="標楷體, 36pt" IsDockedInsideChartArea="False" Text="Transducer 零件失效機率">
                    </asp:Title>
                </Titles>
            </asp:Chart>
            <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Transducer]"></asp:SqlDataSource>

        </div>
    </form>
</body>
</html>
