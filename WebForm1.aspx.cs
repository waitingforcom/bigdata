﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.UI.DataVisualization.Charting;

namespace BigData
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        // 歷史分析載入頁面
        protected void Page_Load(object sender, EventArgs e)
        {
            // 如果使用者的登入狀態是空的，表示閒置太久或沒有登入
            if (Session["isLogin"] == null)
            {
                // 轉至登入頁面
                Response.Redirect("Login.aspx");
            }
            // 如果是第一次載入
            if (!IsPostBack)
            {
                // 連接資料庫
                SqlConnection connection = OpenSQLConnection();
                // 取得設定中的密碼
                GetPWD(connection);
                // 取得設定中，歷史分析年份起始設定的值
                int timePeriod = GetDefaultYear(connection);
                // 設定與現在年份的差異
                int timeDifferenceFromNow = 0;
                // 設定各歷史分析的圖表
                SetChart(timePeriod, timeDifferenceFromNow, "BEC", BEC_Chart);
                SetChart(timePeriod, timeDifferenceFromNow, "A1", A1_Chart);
                SetChart(timePeriod, timeDifferenceFromNow, "A2", A2_Chart);
                SetChart(timePeriod, timeDifferenceFromNow, "A3", A3_Chart);
                SetChart(timePeriod, timeDifferenceFromNow, "Transducer", Transducer_Chart);
            }
        }

        private void SetChart(int timePeriod, int timeDifferenceFromNow, string className, Chart chart)
        {
            // 連接資料庫
            SqlConnection connection = OpenSQLConnection();
            // 清空資料庫的table以便存入新的資料
            Truncate_Table(connection, className);
            // 用R取得各年份零件失效機率
            string output = GetRoutput(timePeriod, timeDifferenceFromNow, className);
            // 處理output取得所有失效機率的清單
            List<float> failedrate_list = GetFailedRate(output);
            // 處理output取得前五名失效的零件
            List<string> component_list = GetComponent(output);
            // 將零件名稱與失效機率依照各年份存入資料庫
            InsertTop5ToDB(connection, timePeriod, timeDifferenceFromNow, failedrate_list, component_list, className);
            // 設定圖表內容(時間,失效零件)
            SetChartInform(timePeriod, component_list, chart);
            // 儲存圖表
            SaveChartAsImage(className, chart);

        }

        // 執行R程式，取得零件名稱與失效機率
        private string GetRoutput(int timePeriod, int timeDifferenceFromNow, string className)
        {
            // 設定R執行檔位置
            var rpath = @"C:\Program Files\R\R-3.4.4\bin\Rscript.exe";
            // 設定要執行的R檔案
            var scriptpath = @"C:\R\LR\" + className + "_history.R";
            // 執行R程式
            var r_output = RunRScript(rpath, scriptpath, timePeriod, timeDifferenceFromNow);
            // 取得R程式執行後的輸出
            string output = ProcessROutput(r_output);
            // 回傳輸出output
            return output;
        }

        // 設定圖表內容資訊
        private void SetChartInform(int timePeriod, List<string> component_list, Chart chart)
        {
            // 設定圖表前五名零件名稱
            SetChartSeriesName(component_list, chart);
            // 設定圖表起末時間
            SetChartTimePeriod(timePeriod, chart);
        }

        // 儲存圖表，名稱為"類別_historyChart.jpg"，例如A1_historyChart.jpg
        private void SaveChartAsImage(string className, Chart chart)
        {
            chart.SaveImage("C:\\R\\LR" + className + "_historyChart.jpg", ChartImageFormat.Jpeg);
        }

        //設定圖表時間
        private void SetChartTimePeriod(int timePeriod, Chart chart)
        {
            //X軸最大年份
            chart.ChartAreas["ChartArea1"].AxisX.Maximum = timePeriod+1;
        }

        // 設定圖表的失效零件名稱
        private void SetChartSeriesName(List<string> component_list, Chart chart)
        {
            // 設定前五名零件失效名稱
            for (int j = 0; j < component_list.Count; j++)
            {
                chart.Series[j].Name = component_list[j];
            }
        }

        // 將零件名稱與失效機率依照各年份存入資料庫
        private static void InsertTop5ToDB(SqlConnection connection, int timePeriod, int timeDifferenceFromNow, List<float> failedrate_list, List<string> component_list, string tableName)
        {
            // 設定今年的年份
            int currentYear = DateTime.Now.Year;
            for (int i = 0; i < timePeriod; i++)
            {
                // 計算開始的年份
                int year = currentYear - timeDifferenceFromNow - (timePeriod - i) + 1;
                // 開啟資料庫連線
                connection.Open();
                // SQL將零件名稱與失效機率依照各年份存入資料庫的語法
                string topcmdText = " INSERT INTO " + tableName + " (top1_name,top1_year,top1_Failure_percent,top2_name,top2_year,top2_Failure_percent,top3_name,top3_year,top3_Failure_percent,top4_name,top4_year,top4_Failure_percent,top5_name,top5_year,top5_Failure_percent) VALUES ( '" + component_list[0] + "','" + year + "','" + failedrate_list[i].ToString() + "','" + component_list[1] + "','" + year + "','" + failedrate_list[i + timePeriod * 1].ToString() + "','" + component_list[2] + "','" + year + "','" + failedrate_list[i + timePeriod * 2].ToString() + "','" + component_list[3] + "','" + year + "','" + failedrate_list[i + timePeriod * 3].ToString() + "','" + component_list[4] + "','" + year + "','" + failedrate_list[i + timePeriod * 4].ToString() + "') ";
                // 新增指令
                SqlCommand topcmd = new SqlCommand(topcmdText, connection);
                // 執行指令
                SqlDataReader topreader = topcmd.ExecuteReader();
                // 關閉資料庫連線
                connection.Close();

            }
        }

        // 更換欲查詢年份
        protected void ConfirmBtn_Click(object sender, EventArgs e)
        {
            try
            {
                // 存取開始年份
                int startYear = int.Parse(StartYear.Text);
                // 存取結束年份
                int endYear = int.Parse(EndYear.Text);
                // 存取現在年份
                int nowYear = int.Parse(DateTime.Now.Year.ToString());
                if (endYear < startYear || endYear > nowYear + 1)
                {
                    // 若錯誤，顯示錯誤訊息
                    var msg = "錯誤 !! 請檢查資料格式";
                    string Alertmsg = "<script language = JavaScript > alert('" + msg + "'); </script>";
                    Response.Write(Alertmsg);
                }
                // 計算總時間(幾年)
                int timePeriod = endYear - startYear + 1;
                // 計算現在與結束年份的時間差
                int timeDifferenceFromNow = DateTime.Now.Year - endYear;
                // 設定各歷史分析的圖表
                SetChart(timePeriod, timeDifferenceFromNow, "BEC", BEC_Chart);
                SetChart(timePeriod, timeDifferenceFromNow, "A1", A1_Chart);
                SetChart(timePeriod, timeDifferenceFromNow, "A2", A2_Chart);
                SetChart(timePeriod, timeDifferenceFromNow, "A3", A3_Chart);
                SetChart(timePeriod, timeDifferenceFromNow, "Transducer", Transducer_Chart);
            }
            catch
            {
                // 若錯誤，顯示錯誤訊息
                var msg = "錯誤 !! 請檢查資料格式";
                string Alertmsg = "<script language = JavaScript > alert('" + msg + "'); </script>";
                Response.Write(Alertmsg);
            }

        }

        // 取得設定中的密碼
        private void GetPWD(SqlConnection connection)
        {
            // 開啟資料庫連線
            connection.Open();
            // SQL 撈取密碼的指令
            var pwdcommand = "SELECT password FROM Settings";
            // 新增資料庫命令
            SqlCommand pwdcmd = new SqlCommand(pwdcommand, connection);
            // 執行命令
            SqlDataReader pwdreader = pwdcmd.ExecuteReader();
            // 讀取密碼
            while (pwdreader.Read())
            {
                PWD.Text = pwdreader.GetString(0);
            }
            // 關閉資料庫連線
            connection.Close();
        }

        // 清空TextBox的值
        private void EmptyYearTextBox()
        {
            StartYear.Text = "";
            EndYear.Text = "";
        }

        // 開啟資料庫連線
        private static SqlConnection OpenSQLConnection()
        {
            // 設定要連線的資料庫及登入的帳密
            String conn = "Server=localhost;User id=LoginAIDC;Password=aidc2019;database=test;";
            // 建立連線
            SqlConnection connection = new SqlConnection(conn);
            // 回傳該連線
            return connection;
        }

        // 執行Rscript執行檔
        private static string RunRScript(string rpath, string scriptpath, int timePeriod, int timeDifferenceFromNow)
        {
            try
            {
                // 存取現在年份
                int currentYear = DateTime.Now.Year;
                // 存取現在年份的第一天
                DateTime firstDateofYear = new DateTime(currentYear, 1, 1);
                // 存取現在的時間
                DateTime now = DateTime.Now;
                // 計算現在時間與今年第一天的天數差
                int diff = now.Subtract(firstDateofYear).Days;
                // 程序需要的資訊
                var info = new ProcessStartInfo
                {
                    FileName = rpath,
                    WorkingDirectory = Path.GetDirectoryName(scriptpath),
                    Arguments = scriptpath + " " + timePeriod + " " + timeDifferenceFromNow + " " + diff,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    CreateNoWindow = true,
                    UseShellExecute = false
                };

                // 新增程序以執行外部程式R
                using (var proc = new Process { StartInfo = info })
                {
                    // 開始程序
                    proc.Start();
                    // 回傳R程式output
                    return proc.StandardOutput.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return string.Empty;
        }

        // 處理R程式產出的output
        private string ProcessROutput(string r_output)
        {
            // 將R程式的輸出做字串處理，以便存進資料庫
            return r_output.Replace("\r\n", "").Replace(" 1 ", "").Replace("  ", " ").Replace("  ", " ");
        }

        // 從R的輸出產生一個含有前五名失效零件的list
        private List<string> GetComponent(string output)
        {
            // 創建component_list存放前五名失效零件件號
            List<string> component_list = new List<string>();
            // 做字串處理整理出零件名稱
            int index = output.IndexOf("[[1]]") + 1;
            var component_name = output.Substring(0, index);
            component_name = component_name.Replace("[1]", "").Replace("[", "").Replace("\"", " ").Trim().Replace("  ", " ").Replace("  ", " ");
            // 將整理完的零件名稱加入component_list
            component_list.AddRange(component_name.Split(' '));
            return component_list;
        }

        // 從R的輸出產生一個含有前五名失效零件在各年的失效機率的list
        private List<float> GetFailedRate(string output)
        {
            // 創建failedrate_list存放所有失效機率
            List<float> failedrate_list = new List<float>();
            // 做字串處理整理出所有失效機率
            int index = output.IndexOf("[[1]]") + 1;
            int length = output.Length - index;
            var failed_rate = output.Substring(index, length);
            // 用正則表達式分割字串取得失效機率
            string pattern = @"\[+\d+\]+";
            var failed_rate_elements = Regex.Split(failed_rate, pattern);
            // 將失效機率做轉換並加到failedrate_list
            for (int i = 1; i < failed_rate_elements.Length; i++)
            {
                failedrate_list.Add(float.Parse(failed_rate_elements[i]) * 100);
            }
            return failedrate_list;
        }

        // 清除資料庫的table
        private static void Truncate_Table(SqlConnection connection, string tableName)
        {
            // 開啟資料庫連線
            connection.Open();
            // SQL清除table語法
            string command = "TRUNCATE TABLE " + tableName;
            // 新增SQL命令
            SqlCommand cmd = new SqlCommand(command, connection);
            // 執行命令
            cmd.ExecuteNonQuery();
            // 關閉資料庫連線
            connection.Close();
        }

        // 取得設定中，歷史分析年份起始設定的值
        private static int GetDefaultYear(SqlConnection connection)
        {
            // 開啟資料庫連線
            connection.Open();
            // SQL拿取語法
            string stryear = " SELECT year FROM WF10_year ";
            // 新增SQL命令
            SqlCommand cmdyear = new SqlCommand(stryear, connection);
            // 執行命令
            SqlDataReader yearreader = cmdyear.ExecuteReader();
            int yeartemp = 0;
            // 讀取年份起始設定的值
            while (yearreader.Read())
            {
                yeartemp = yearreader.GetInt32(0);
            }
            // 關閉資料庫連線
            connection.Close();
            // 回傳年份起始設定的值
            return yeartemp;
        }
    }
}