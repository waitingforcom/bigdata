﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace BigData
{
    public partial class WebForm8 : System.Web.UI.Page
    {
        // 管理頁面載入頁面
        protected void Page_Load(object sender, EventArgs e)
        {
            // 如果使用者的登入狀態是空的，表示閒置太久或沒有登入
            if (Session["isLogin"] == null)
            {
                // 轉至登入頁面
                Response.Redirect("Login.aspx");
            }
            // 如果是第一次載入
            if (!IsPostBack)
            {
                SqlConnection connection = OpenSQLConnection();
                // 取得生命週期和失效機率設定值
                int defaultLifecycle, defaultFailedRate;
                GetDefaultSettings(connection, out defaultLifecycle, out defaultFailedRate);
                RemindTime.Text = defaultLifecycle.ToString();
                FailedRate.Text = defaultFailedRate.ToString();
                // 取得設定中，歷史分析年份起始設定的值
                GetDefaultYear(connection);
            }
        }

        // 連接資料庫
        private static SqlConnection OpenSQLConnection()
        {
            String conn = "Server=localhost;User id=LoginAIDC;Password=aidc2019;database=test;";
            SqlConnection connection = new SqlConnection(conn);
            connection.Open();
            return connection;
        }

        // 觸發BEC診斷參數修改按鈕
        protected void BECConfirmBtn_Click(object sender, EventArgs e)
        {
            SqlConnection connection = OpenSQLConnection();
            string command;
            try
            {
                BECErrorMsg.Visible = false;
                string message;
                // 檢查生命週期和失效機率是否為空
                bool IsLifecyclenull = string.IsNullOrEmpty(RemindTime.Text);
                bool IsFailedratenull = string.IsNullOrEmpty(FailedRate.Text);
                // 如果都是空的就不做事
                if (IsLifecyclenull && IsFailedratenull)
                {
                    connection.Close();
                }　
                else if (IsLifecyclenull)　// 如果生命週期是空的
                {
                    // 檢查失效機率格式
                    int failedrate = Int32.Parse(FailedRate.Text);
                    if (0 > failedrate || failedrate > 100)
                        EmptyFailedrate();
                    else　// 更新失效機率
                        UpdateFailedrate(connection, out command, out message, failedrate);
                }　
                else if (IsFailedratenull)　// 如果失效機率是空的
                {
                    // 檢查生命週期格式並更新生命週期
                    int lifecycle = Int32.Parse(RemindTime.Text);
                    UpdateLifecycle(connection, out command, out message, lifecycle);
                }
                else // 如果兩個都不是空的
                {
                    // 檢查資料格式
                    int lifecycle = Int32.Parse(RemindTime.Text);
                    int failedrate = Int32.Parse(FailedRate.Text);
                    if (0 > failedrate || failedrate > 100)
                        EmptyFailedrate();
                    else　// 更新生命週期和失效機率
                        UpdateLifecycleAndFailedRate(connection, out command, out message, lifecycle, failedrate);
                }
                connection.Close();
                EmptyBECTextBox();

            }
            catch
            {
                // 顯示錯誤訊息
                BECErrorMsg.Text = "錯誤 !! 請檢查資料格式";
                BECErrorMsg.Visible = true;
                EmptyBECTextBox();
            }
        }

        // 觸發資料備份按鈕
        protected void HistoryConfirmBtn_Click(object sender, EventArgs e)
        {
            try
            {
                // 檢查資料格式與邏輯
                int startYear = int.Parse(StartYear.Text);
                int endYear = int.Parse(EndYear.Text);
                int nowYear = int.Parse(DateTime.Now.Year.ToString());
                if (endYear < startYear || endYear > nowYear + 1)
                {
                    // 若錯誤，顯示錯誤訊息
                    DBErrorMsg.Text = "錯誤 !! 請檢查年份範圍";
                    DBErrorMsg.Visible = true;
                    EmptyYearTextBox();
                }
                else
                {
                    // 創建資料表的名稱為history加現在時間
                    var now = DateTime.Now.ToString("yyyyMMddHHmmss");
                    string tableName = "history" + now;
                    // 跳出提醒訊息
                    var msg = "確定要將資料移除並備份?";
                    string Alertmsg = "<script language = JavaScript > alert('" + msg + "'); </script>";
                    Response.Write(Alertmsg);
                    // 創建歷史資料備份的資料表
                    CreateHistoryTable(tableName);
                    // 將備份的歷史資料匯入新的資料表中
                    InsertToHistoryTable(tableName);
                    // 刪除備份的歷史資料
                    DeleteHistoryData();
                    // 產生備份的歷史資料csv檔，位置在F槽底下
                    GenerateHistoryFile(tableName);
                }
            }
            catch
            {
                // 顯示錯誤訊息
                DBErrorMsg.Text = "錯誤 !! 請檢查資料格式";
                DBErrorMsg.Visible = true;
                // 清空起末年份欄位
                EmptyYearTextBox();
            }
        }

        // 清空起末年份欄位
        private void EmptyYearTextBox()
        {
            StartYear.Text = "";
            EndYear.Text = "";
        }

        // 將備份的歷史資料匯入新的資料表中
        private void InsertToHistoryTable(string tableName)
        {
            SqlConnection connection = OpenSQLConnection();
            string command = "INSERT INTO [dbo].[" + tableName + "] SELECT * FROM final_new where year([Receipt_Date]) BETWEEN " + StartYear.Text + " AND " + EndYear.Text;
            SqlCommand cmd = new SqlCommand(command, connection);
            cmd.ExecuteNonQuery();
            connection.Close();
        }

        // 刪除備份的歷史資料
        private void DeleteHistoryData()
        {
            SqlConnection connection = OpenSQLConnection();
            string command = "DELETE FROM [dbo].[final_new] WHERE year([Receipt_Date]) BETWEEN " + StartYear.Text + " AND " + EndYear.Text;
            SqlCommand cmd = new SqlCommand(command, connection);
            cmd.ExecuteNonQuery();
            connection.Close();
        }
        
        // 產生備份的歷史資料csv檔
        private static void GenerateHistoryFile(string tableName)
        {
            SqlConnection connection = OpenSQLConnection();
            String command = "SELECT * FROM " + tableName;
            SqlCommand cmd = new SqlCommand(command, connection);
            SqlDataReader reader = cmd.ExecuteReader();
            using (DataTable dt = new DataTable())
            {
                dt.Load(reader);
                Console.WriteLine(dt.Rows.Count);
                StreamWriter CsvfileWriter = new StreamWriter(@"C:\" + tableName + ".csv", false, Encoding.UTF8);
                // 寫入csv
                using (CsvfileWriter)
                {
                    CsvfileWriter.WriteLine(string.Join(",", dt.Columns.Cast<DataColumn>().Select(csvfile => csvfile.ColumnName)));
                    foreach (DataRow row in dt.Rows)
                    {
                        CsvfileWriter.WriteLine(string.Join(",", row.ItemArray));
                    }
                }
            }
            connection.Close();
        }

        // 創建歷史資料備份的資料表
        private static void CreateHistoryTable(string tableName)
        {
            SqlConnection connection = OpenSQLConnection();
            string command = "CREATE TABLE[dbo].[" + tableName + "] ( [Work_number][int] NULL,[BEC_Serial_number][nvarchar](50) NULL,[BEC_Part_Number] [nvarchar] (50) NULL,[BEC_completion_Date] [char](10) NULL,[Receipt_Date] [char](10) NULL,[Ship_Date] [char](10) NULL,[Fault_Description] [nvarchar] (100) NULL," +
               "[CHECKLIST_Description] [nvarchar] (150) NULL,[Detect_Failure_Record] [nvarchar] (150) NULL,[Components_number] [nvarchar] (50) NULL,[Components_name] [nvarchar] (50) NULL,[number] [tinyint] NULL,[counter][smallint] NOT NULL,CONSTRAINT[PK_" + tableName + "] " +
               "PRIMARY KEY CLUSTERED([counter] ASC)WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON[PRIMARY]) ON[PRIMARY]";
            SqlCommand cmd = new SqlCommand(command, connection);
            cmd.ExecuteNonQuery();
            connection.Close();
        }

        // 清空BEC診斷欄位
        private void EmptyBECTextBox()
        {
            RemindTime.Text = "";
            FailedRate.Text = "";
        }

        // 更新生命週期和失效機率
        protected void UpdateLifecycleAndFailedRate(SqlConnection connection, out string delcommand, out string message, int lifecycle, int failedrate)
        {
            delcommand = "UPDATE Settings SET lifecycle =" + lifecycle + ",Failure_percent = " + failedrate;
            ExecuteQuery(connection, delcommand);
            // 跳出更新成功訊息
            message = "更新成功, 更換提醒 < " + lifecycle + " 天, 失效機率 > " + failedrate + " %";
            string Alertmsg = "<script language = JavaScript > alert('" + message + "'); </script>";
            Response.Write(Alertmsg);
        }

        // 更新生命週期
        protected void UpdateLifecycle(SqlConnection connection, out string delcommand, out string message, int lifecycle)
        {
            delcommand = "UPDATE Settings SET lifecycle =" + lifecycle;
            ExecuteQuery(connection, delcommand);
            // 跳出更新成功訊息
            message = "更新成功, 更換提醒 < " + lifecycle + " 天";
            string Alertmsg = "<script language = JavaScript > alert('" + message + "'); </script>";
            Response.Write(Alertmsg);
        }

        // 更新失效機率
        protected void UpdateFailedrate(SqlConnection connection, out string delcommand, out string message, int failedrate)
        {
            delcommand = "UPDATE Settings SET Failure_percent =" + failedrate;
            ExecuteQuery(connection, delcommand);
            // 跳出更新成功訊息
            message = "更新成功, 失效機率 > " + failedrate + " %";
            string Alertmsg = "<script language = JavaScript > alert('" + message + "'); </script>";
            Response.Write(Alertmsg);
        }

        // 顯示錯誤訊息並清空失效機率欄位
        private void EmptyFailedrate()
        {
            BECErrorMsg.Text = "失效機率需介於0~100之間";
            BECErrorMsg.Visible = true;
            FailedRate.Text = "";
        }

        // 執行指令
        private static void ExecuteQuery(SqlConnection connection, string command)
        {
            SqlCommand cmd = new SqlCommand(command, connection);
            cmd.ExecuteNonQuery();
            connection.Close();
        }

        // 取得生命週期和失效機率設定值
        private static void GetDefaultSettings(SqlConnection connection, out int defaultLifecycle, out int defaultFailedRate)
        {
            string cmd = "SELECT lifecycle,Failure_percent FROM Settings";
            SqlCommand tempcmd = new SqlCommand(cmd, connection);
            SqlDataReader reader = tempcmd.ExecuteReader();
            defaultLifecycle = 0;
            defaultFailedRate = 0;
            while (reader.Read())
            {
                defaultLifecycle = reader.GetInt32(0);
                defaultFailedRate = reader.GetInt32(1);
            }
            reader.Close();
            connection.Close();
        }

        // 取得設定中，歷史分析年份起始設定的值
        private void GetDefaultYear(SqlConnection connection)
        {
            connection.Open();
            var command = "SELECT year FROM WF10_year";
            SqlCommand pwdcmd = new SqlCommand(command, connection);
            SqlDataReader reader = pwdcmd.ExecuteReader();
            while (reader.Read())
            {
                // 存取該值顯示於頁面
                HistoryYear.Text = reader.GetInt32(0).ToString();
            }
            connection.Close();
        }

        // 觸發新增使用者按鈕
        protected void AddNewUser_Click(object sender, EventArgs e)
        {
            // 連接資料庫
            SqlConnection connection = OpenSQLConnection();
            string newUserAccount = NewUserAccount.Text.ToString();
            string newUserPassword = NewUserPassword.Text.ToString();
            string newUserName = NewUserName.Text.ToString();
            // 檢查新用戶密碼格式
            if (newUserPassword.Length != 6)
            {
                Response.Write("<script language=JavaScript> alert('密碼須為6位數字'); </script>");
                return;
            }
            // 檢查新用戶資料是否齊全
            if (string.IsNullOrEmpty(newUserAccount) || string.IsNullOrEmpty(newUserPassword) || string.IsNullOrEmpty(newUserName) || UserList.SelectedValue == "")
            {
                var msg = "請填上所有新使用者資訊";
                string Alertmsg = "<script language = JavaScript > alert('" + msg + "'); </script>";
                Response.Write(Alertmsg);
                return;
            }
            // 存取新用戶是否為管理者
            int isAdmin = int.Parse(UserList.SelectedValue);
            // 將新用戶匯入資料庫
            string command = "INSERT INTO UserInform (Account, Password, UserName, IsAdmin) VALUES ('" + newUserAccount + "','" + newUserPassword + "',N'" + newUserName + "'," + isAdmin + ");";
            SqlCommand cmd = new SqlCommand(command, connection);
            cmd.ExecuteNonQuery();
            connection.Close();
        }

        // 觸發編輯使用者按鈕
        protected void EditUser_Click(object sender, EventArgs e)
        {
            // 做管理頁面收放
            if(GridView1.Visible == true)
            {
                GridView1.Visible = false;
                EditUserBtn.Text = "使用者管理";
            }
            else
            {
                GridView1.Visible = true;
                EditUserBtn.Text = "管理完成";
            }
        }

        // 觸發更改密碼按鈕
        protected void PwdButton_Click(object sender, EventArgs e)
        {
            // 連接資料庫
            SqlConnection connection = OpenSQLConnection();
            // 新的密碼不能為空
            if (string.IsNullOrEmpty(Password.Text))
            {
                connection.Close();
            }
            else
            {
                // 更新資料庫中的密碼
                string command = "UPDATE Settings SET password = '" + Password.Text + "'";
                ExecuteQuery(connection, command);
                // 跳出更新成功訊息
                string message = "更新密碼成功" ;
                string Alertmsg = "<script language = JavaScript > alert('" + message + "'); </script>";
                Response.Write(Alertmsg);
                Password.Text = "";
            }
        }

        // 觸發歷史分析按鈕
        protected void HistoryBtn_Click(object sender, EventArgs e)
        {
            // 連接資料庫
            SqlConnection connection = OpenSQLConnection();
            // 判斷更新的值不可為空
            if (string.IsNullOrEmpty(HistoryYear.Text))
            {
                HistoryErrorMsg.Text = "不可為空";
                HistoryErrorMsg.Visible = true;
                connection.Close();
            }
            else
            {
                try
                {
                    // 檢查更新的值格式
                    int year = Int32.Parse(HistoryYear.Text);
                    // 更新資料庫中的值
                    string command = "UPDATE WF10_year SET year = '" + year + "'";
                    ExecuteQuery(connection, command);
                    // 跳出更新成功訊息
                    string message = "更新成功 歷史分析 : " + year + " 年 ";
                    string Alertmsg = "<script language = JavaScript > alert('" + message + "'); </script>";
                    Response.Write(Alertmsg);

                    // 清空該欄位
                    HistoryYear.Text = "";
                    connection.Close();
                }
                catch
                {
                    //若格式錯誤，顯示錯誤訊息
                    HistoryErrorMsg.Text = "請填入數字";
                    HistoryErrorMsg.Visible = true;
                }
            }
        }
    }
}