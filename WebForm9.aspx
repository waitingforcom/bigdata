﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm9.aspx.cs" Inherits="BigData.WebForm9" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title></title>
    <style type="text/css">
        .auto-style1 {
            text-align: center;
            font-family: DFKai-sb,"標楷體";
        }

        .auto-style2 {
            font-family: DFKai-sb,"標楷體";
        }

        .GridViewEditRow input[type=text] {
            width: 150px;
        }
    </style>
    <link href="layout.css" rel="stylesheet" type="text/css" />
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/jquery-ui.css" rel="stylesheet" />
    <script src="Scripts/jquery-1.10.2.js"></script>
    <script src="Scripts/jquery-ui.js"></script>
    <script type="text/javascript" src="main.js"></script>
</head>


<script>
    // 顯示讀取遮罩
    function ShowProgressBar() {
        displayProgress();
        displayMaskFrame();
    }

    // 隱藏讀取遮罩
    function HideProgressBar() {
        var progress = $('#divProgress');
        var maskFrame = $("#divMaskFrame");
        progress.hide();
        maskFrame.hide();
    }
    // 顯示讀取畫面
    function displayProgress() {
        var w = $(document).width();
        var h = $(window).height();
        var progress = $('#divProgress');
        progress.css({ "z-index": 999999, "top": 400, "left": 700 });
        progress.show();
    }
    // 顯示遮罩畫面
    function displayMaskFrame() {
        var w = $(window).width();
        var h = $(document).height();
        var maskFrame = $("#divMaskFrame");
        maskFrame.css({ "z-index": 999998, "opacity": 0.7, "width": w, "height": h });
        maskFrame.show();
    }
</script>
<body>
    <form id="form1" runat="server">

        <div class="container-fluid img-fluid">
            <asp:Image ID="img_Title" runat="server" ImageUrl="~/title1.png" />
            <p hidden>
                <asp:Label ID="PWD" runat="server"></asp:Label>
            </p>
        </div>

        <div id="NAV" class="container-fluid">
            <ul class="navbar-nav nav-fill w-100 d-inline-block">
                <li class="w-25"><a href="WebForm4.aspx">零件診斷</a></li>
                <li class="w-25" onclick="toggle()">
                    <a>新增維修紀錄 <span class="caret"></span></a>
                    <ul id="Dropdownlist" style="display: none;">
                        <li><a href="WebForm2.aspx">新增單筆維修紀錄</a></li>
                        <li><a href="WebForm3.aspx">新增多筆維修紀錄</a></li>
                    </ul>
                </li>
                <li class="w-25"><a class="active" href="WebForm9.aspx">資料庫功能</a></li>
                <%--<li><a href="WebForm1.aspx"><font size="6">歷史分析</font></a></li>--%>
                <li class="w-25"><a onclick="inputPassword()">管理頁面</a></li>
            </ul>
        </div>

        <div id="CONTENT" style="background-color: transparent; margin: 0 80px 0 80px; width: 95vw">
            <p class="auto-style2" align="center">
                <asp:Button ID="TrainBtn" runat="server" Height="57px" OnClick="TrainBtn_Click" OnClientClick="ShowProgressBar();" Text=" 大數據模型更新 " />
            </p>
            <div id="divProgress" style="text-align: center; display: none; position: fixed; top: 30%; left: 30%;">
                <asp:Image ID="imgLoading" runat="server" ImageUrl="~/Images/loading.gif" Width="300" />
                <br />
                <font color="#1B3563" size="6px" class="auto-style2">模型更新中...請勿離開</font>
                <br />
                <font color="#1B3563" size="6px" class="auto-style2">若要終止更新，請按重整頁面離開</font>
                <br />
                <asp:Label ID="TimeSpan" runat="server" Width="605px" class="auto-style2"></asp:Label>
            </div>
            <div id="divMaskFrame" style="background-color: #F2F4F7; display: none; left: 0px; position: absolute; top: 0px;">
            </div>
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="counter" DataSourceID="SqlDataSource1" Style="margin-right: auto;"
                CssClass="gvStyle" HeaderStyle-CssClass="gv-header" AlternatingRowStyle-CssClass="alt" PagerStyle-CssClass="pgr" Width="2400px">
                <HeaderStyle Wrap="False" />
                <EditRowStyle BackColor="PaleGoldenRod " CssClass="GridViewEditRow" />
                <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
                <Columns>
                    <asp:TemplateField ShowHeader="False">
                        <EditItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="更新" Font-Size="15pt" OnClientClick="return confirm('確認要修改嗎？');"></asp:LinkButton>
                            &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="取消" Font-Size="15pt"></asp:LinkButton>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit" Text="編輯" Font-Size="15pt"></asp:LinkButton>
                            &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Delete" Text="刪除" Font-Size="15pt" OnClientClick="return confirm('確認要刪除嗎？');"></asp:LinkButton>
                        </ItemTemplate>
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Width="100px" />
                    </asp:TemplateField>
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="counter" HeaderText="編號" ReadOnly="True" SortExpression="counter">
                        <ItemStyle Width="60px" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Font-Size="15pt" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Work_number" HeaderText="工單號碼" SortExpression="Work_number">
                        <ItemStyle Width="100px" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Font-Size="15pt" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="BEC_Serial_number" HeaderText="BEC序號" SortExpression="BEC_Serial_number">
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Width="100px" Font-Size="15pt" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="BEC_Part_Number" HeaderText="BEC件號" SortExpression="BEC_Part_Number">
                        <ItemStyle Width="100px" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Font-Size="15pt" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="BEC_completion_Date" HeaderText="BEC完工" SortExpression="BEC_completion_Date">
                        <ItemStyle Width="100px" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Font-Size="15pt" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Receipt_Date" HeaderText="接收日期" SortExpression="Receipt_Date">
                        <ItemStyle Width="100px" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Font-Size="15pt" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Ship_Date" HeaderText="出貨日期" SortExpression="Ship_Date">
                        <ItemStyle Width="100px" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Font-Size="15pt" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Fault_Description" HeaderText="掛簽缺點描述" SortExpression="Fault_Description">
                        <ItemStyle Width="250px" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Font-Size="15pt" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="CHECKLIST_Description" HeaderText="CHECKLIST_描述" SortExpression="CHECKLIST_Description">
                        <ItemStyle Width="200px" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Font-Size="15pt" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Detect_Failure_Record" HeaderText="失效檢測紀錄" SortExpression="Detect_Failure_Record">
                        <ItemStyle Width="300px" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Font-Size="15pt" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Components_number" HeaderText="零件件號" SortExpression="Components_number">
                        <ItemStyle Width="120px" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Font-Size="15pt" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Components_name" HeaderText="零件中文名稱" SortExpression="Components_name">
                        <ItemStyle Width="120px" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Font-Size="15pt" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="number" HeaderText="數量" SortExpression="number">
                        <ItemStyle Width="50px" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Font-Size="15pt" HorizontalAlign="Right" />
                    </asp:BoundField>
                </Columns>
                <PagerSettings Mode="NumericFirstLast"
                    FirstPageText="First"
                    LastPageText="Last"
                    PageButtonCount="5"
                    Position="Bottom" />
                <PagerStyle CssClass="pgr"></PagerStyle>
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" InsertCommand="INSERT INTO [final_new] ([Work_number], [BEC_Serial_number], [BEC_Part_Number], [BEC_completion_Date], [Receipt_Date], [Ship_Date], [Fault_Description], [CHECKLIST_Description], [Detect_Failure_Record], [Components_number], [Components_name], [number], [counter]) VALUES (@Work_number, @BEC_Serial_number, @BEC_Part_Number, @BEC_completion_Date, @Receipt_Date, @Ship_Date, @Fault_Description, @CHECKLIST_Description, @Detect_Failure_Record, @Components_number, @Components_name, @number, @counter)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [final_new]" ConflictDetection="CompareAllValues"
                DeleteCommand="DELETE FROM [final_new] WHERE [counter] = @original_counter"
                UpdateCommand="UPDATE [final_new] SET [Work_number] = @Work_number, [BEC_Serial_number] = @BEC_Serial_number, [BEC_Part_Number] = @BEC_Part_Number, [BEC_completion_Date] = @BEC_completion_Date, [Receipt_Date] = @Receipt_Date, [Ship_Date] = @Ship_Date, [Fault_Description] = @Fault_Description, [CHECKLIST_Description] = @CHECKLIST_Description, [Detect_Failure_Record] = @Detect_Failure_Record, [Components_number] = @Components_number, [Components_name] = @Components_name, [number] = @number WHERE [counter] = @original_counter AND (([Work_number] = @original_Work_number) OR ([Work_number] IS NULL AND @original_Work_number IS NULL)) AND (([BEC_Serial_number] = @original_BEC_Serial_number) OR ([BEC_Serial_number] IS NULL AND @original_BEC_Serial_number IS NULL)) AND (([BEC_Part_Number] = @original_BEC_Part_Number) OR ([BEC_Part_Number] IS NULL AND @original_BEC_Part_Number IS NULL)) AND (([BEC_completion_Date] = @original_BEC_completion_Date) OR ([BEC_completion_Date] IS NULL AND @original_BEC_completion_Date IS NULL)) AND (([Receipt_Date] = @original_Receipt_Date) OR ([Receipt_Date] IS NULL AND @original_Receipt_Date IS NULL)) AND (([Ship_Date] = @original_Ship_Date) OR ([Ship_Date] IS NULL AND @original_Ship_Date IS NULL)) AND (([Fault_Description] = @original_Fault_Description) OR ([Fault_Description] IS NULL AND @original_Fault_Description IS NULL)) AND (([CHECKLIST_Description] = @original_CHECKLIST_Description) OR ([CHECKLIST_Description] IS NULL AND @original_CHECKLIST_Description IS NULL)) AND (([Detect_Failure_Record] = @original_Detect_Failure_Record) OR ([Detect_Failure_Record] IS NULL AND @original_Detect_Failure_Record IS NULL)) AND (([Components_number] = @original_Components_number) OR ([Components_number] IS NULL AND @original_Components_number IS NULL)) AND (([Components_name] = @original_Components_name) OR ([Components_name] IS NULL AND @original_Components_name IS NULL)) AND (([number] = @original_number) OR ([number] IS NULL AND @original_number IS NULL))">
                <DeleteParameters>
                    <asp:Parameter Name="original_counter" Type="Int16" />
                    <asp:Parameter Name="original_Work_number" Type="Int32" />
                    <asp:Parameter Name="original_BEC_Serial_number" Type="String" />
                    <asp:Parameter Name="original_BEC_Part_Number" Type="String" />
                    <asp:Parameter Name="original_BEC_completion_Date" Type="String" />
                    <asp:Parameter Name="original_Receipt_Date" Type="String" />
                    <asp:Parameter Name="original_Ship_Date" Type="String" />
                    <asp:Parameter Name="original_Fault_Description" Type="String" />
                    <asp:Parameter Name="original_CHECKLIST_Description" Type="String" />
                    <asp:Parameter Name="original_Detect_Failure_Record" Type="String" />
                    <asp:Parameter Name="original_Components_number" Type="String" />
                    <asp:Parameter Name="original_Components_name" Type="String" />
                    <asp:Parameter Name="original_number" Type="Byte" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="Work_number" Type="Int32" />
                    <asp:Parameter Name="BEC_Serial_number" Type="String" />
                    <asp:Parameter Name="BEC_Part_Number" Type="String" />
                    <asp:Parameter Name="BEC_completion_Date" Type="String" />
                    <asp:Parameter Name="Receipt_Date" Type="String" />
                    <asp:Parameter Name="Ship_Date" Type="String" />
                    <asp:Parameter Name="Fault_Description" Type="String" />
                    <asp:Parameter Name="CHECKLIST_Description" Type="String" />
                    <asp:Parameter Name="Detect_Failure_Record" Type="String" />
                    <asp:Parameter Name="Components_number" Type="String" />
                    <asp:Parameter Name="Components_name" Type="String" />
                    <asp:Parameter Name="number" Type="Byte" />
                    <asp:Parameter Name="counter" Type="Int16" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Work_number" Type="Int32" />
                    <asp:Parameter Name="BEC_Serial_number" Type="String" />
                    <asp:Parameter Name="BEC_Part_Number" Type="String" />
                    <asp:Parameter Name="BEC_completion_Date" Type="String" />
                    <asp:Parameter Name="Receipt_Date" Type="String" />
                    <asp:Parameter Name="Ship_Date" Type="String" />
                    <asp:Parameter Name="Fault_Description" Type="String" />
                    <asp:Parameter Name="CHECKLIST_Description" Type="String" />
                    <asp:Parameter Name="Detect_Failure_Record" Type="String" />
                    <asp:Parameter Name="Components_number" Type="String" />
                    <asp:Parameter Name="Components_name" Type="String" />
                    <asp:Parameter Name="number" Type="Byte" />
                    <asp:Parameter Name="original_counter" Type="Int16" />
                    <asp:Parameter Name="original_Work_number" Type="Int32" />
                    <asp:Parameter Name="original_BEC_Serial_number" Type="String" />
                    <asp:Parameter Name="original_BEC_Part_Number" Type="String" />
                    <asp:Parameter Name="original_BEC_completion_Date" Type="String" />
                    <asp:Parameter Name="original_Receipt_Date" Type="String" />
                    <asp:Parameter Name="original_Ship_Date" Type="String" />
                    <asp:Parameter Name="original_Fault_Description" Type="String" />
                    <asp:Parameter Name="original_CHECKLIST_Description" Type="String" />
                    <asp:Parameter Name="original_Detect_Failure_Record" Type="String" />
                    <asp:Parameter Name="original_Components_number" Type="String" />
                    <asp:Parameter Name="original_Components_name" Type="String" />
                    <asp:Parameter Name="original_number" Type="Byte" />
                </UpdateParameters>
            </asp:SqlDataSource>
        </div>
        <p></p>
        <p></p>
    </form>
</body>
</html>
