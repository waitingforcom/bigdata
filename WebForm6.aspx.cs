﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace BigData
{
    public partial class WebForm6 : System.Web.UI.Page
    {
        // 產生報表載入頁面
        protected void Page_Load(object sender, EventArgs e)
        {
            // 如果使用者的登入狀態是空的，表示閒置太久或沒有登入
            if (Session["isLogin"] == null)
            {
                // 轉至登入頁面
                Response.Redirect("Login.aspx");
            }
            // 如果是第一次載入
            if (!IsPostBack)
            {
                // 取得產生report的參數，使用者名稱
                List<ReportParameter> parameters = new List<ReportParameter>();
                parameters.Add(new ReportParameter("UserName", Session["Username"].ToString()));
                parameters.Add(new ReportParameter("UUT_Name", Session["WorkStation"].ToString()));
                //parameters.Add(new ReportParameter("UserName", "AIDC")); //20200803 改成固定使用者名稱AIDC
                // 將參數綁到reportViewer1
                this.ReportViewer1.LocalReport.SetParameters(parameters);

                // 取得設定中的密碼
                SqlConnection connection = OpenSQLConnection();
                var pwdcommand = "SELECT password FROM Settings";
                SqlCommand pwdcmd = new SqlCommand(pwdcommand, connection);
                SqlDataReader pwdreader = pwdcmd.ExecuteReader();
                while (pwdreader.Read())
                {
                    PWD.Text = pwdreader.GetString(0);
                }
                connection.Close();
            }
        }

        // 開啟資料庫連線
        private static SqlConnection OpenSQLConnection()
        {
            String conn = "Server=localhost;User id=LoginAIDC;Password=aidc2019;database=test;";
            SqlConnection connection = new SqlConnection(conn);
            connection.Open();
            return connection;
        }
    }
}