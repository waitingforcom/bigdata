﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm3.aspx.cs" Inherits="BigData.WebForm3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title></title>
    <style type="text/css">
        .auto-style1 {
            text-align: center;
            font-size: xx-large;
            font-family: DFKai-sb,"標楷體";
        }

        .auto-style2 {
            font-size: x-large;
            font-family: DFKai-sb,"標楷體";
        }

        .auto-style3 {
            text-align: left;
            font-size: x-large;
            font-family: DFKai-sb,"標楷體";
        }
    </style>
    <link href="layout.css" rel="stylesheet" type="text/css" />
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <script type="text/javascript" src="main.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container-fluid img-fluid">
            <asp:Image ID="img_Title" runat="server" ImageUrl="~/title1.png" />
            <p hidden>
                <asp:Label ID="PWD" runat="server"></asp:Label>
            </p>
        </div>
        <div id="NAV" class="container-fluid">
            <ul class="navbar-nav nav-fill w-100 d-inline-block">
                <li class="w-25"><a href="WebForm4.aspx">零件診斷</a></li>
                <li class="w-25" onclick="toggle()">
                    <a class="active">新增維修紀錄 <span class="caret"></span></a>
                    <ul id="Dropdownlist" style="display: none;">
                        <li><a href="WebForm2.aspx">新增單筆維修紀錄</a></li>
                        <li><a href="WebForm3.aspx">新增多筆維修紀錄</a></li>
                    </ul>
                </li>
                <li class="w-25"><a href="WebForm9.aspx">資料庫功能</a></li>
                <%--<li><a href="WebForm1.aspx"><font size="6">歷史分析</font></a></li>--%>
                <li class="w-25"><a onclick="inputPassword()">管理頁面</a></li>
            </ul>
        </div>
        <div id="CONTENT" class="auto-style3" style="background-color: transparent; margin: 0 80px">
            <p class="auto-style1">
                <div align="center" style="color: aliceblue">
                    <asp:FileUpload ID="FileUpload1" runat="server" Width="500px" class="auto-style3" Font-Size="XX-Large" />
                </div>
            </p>
            <div align="center" class="auto-style1">
                <asp:Button ID="button1" runat="server" OnClick="PreviewBtn_Click" Text="預覽" color="black" />
            </div>
            <br />

            <br />
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Height="115px">
                <Columns>
                    <asp:BoundField DataField="Work_number" HeaderText="工單號碼" SortExpression="Work_number">
                        <ItemStyle Width="100px" Font-Size="15pt" />
                    </asp:BoundField>
                    <asp:BoundField DataField="BEC_Serial_number" HeaderText="序號" SortExpression="BEC_Serial_number">
                        <ItemStyle Width="100px" Font-Size="15pt" />
                    </asp:BoundField>
                    <asp:BoundField DataField="BEC_Part_Number" HeaderText="件號" SortExpression="BEC_Part_Number">
                        <ItemStyle Width="110px" Font-Size="15pt" />
                    </asp:BoundField>
                    <asp:BoundField DataField="BEC_completion_Date" HeaderText="完工日期" SortExpression="BEC_completion_Date">
                        <ItemStyle Width="110px" Font-Size="15pt" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Receipt_Date" HeaderText="接收日期" SortExpression="Receipt_Date">
                        <ItemStyle Width="110px" Font-Size="15pt" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Ship_Date" HeaderText="出貨日期" SortExpression="Ship_Date">
                        <ItemStyle Width="110px" Font-Size="15pt" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Fault_Description" HeaderText="缺點描述" SortExpression="Fault_Description">
                        <ItemStyle Width="280px" Font-Size="15pt" />
                    </asp:BoundField>
                    <asp:BoundField DataField="CHECKLIST_Description" HeaderText="CHECKLIST描述" SortExpression="CHECKLIST_Description">
                        <ItemStyle Width="280px" Font-Size="15pt" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Detect_Failure_Record" HeaderText="檢測失效紀錄" SortExpression="Detect_Failure_Record">
                        <ItemStyle Width="280px" Font-Size="15pt" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Components_number" HeaderText="零件件號" SortExpression="Components_number">
                        <ItemStyle Width="100px" Font-Size="15pt" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Components_name" HeaderText="零件名稱" SortExpression="Components_name">
                        <ItemStyle Width="150px" Font-Size="15pt" />
                    </asp:BoundField>
                    <asp:BoundField DataField="number" HeaderText="數量" SortExpression="number">
                        <ItemStyle Width="50px" Font-Size="15pt" HorizontalAlign="Right" />
                    </asp:BoundField>
                </Columns>
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [TempData]"></asp:SqlDataSource>
            <p>
                &nbsp;
            </p>
            <div align="center" class="auto-style1">
                <asp:Button ID="SendBtn" runat="server" OnClick="SendBtn_Click" Text="送出" Visible="False" />
            </div>
        </div>
    </form>
</body>
</html>
