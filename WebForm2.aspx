﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="BigData.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>零件維修大數據統計系統</title>
    <style type="text/css">
        .auto-style1 {
            text-align: center;
            font-size: x-large;
            font-family: DFKai-sb,"標楷體";
        }

        .auto-style3 {
            text-align: left;
            font-size: xx-large;
            font-family: DFKai-sb,"標楷體";
        }

        input {
            height: 40px;
            padding: 0 20px;
        }

            input, textarea,
            input::-webkit-input-placeholder,
            textarea::-webkit-input-placeholder {
                font-size: inherit !important;
                line-height: 5;
            }

        .auto-style4 {
            font-size: xx-large;
            font-family: DFKai-sb,"標楷體";
        }

        .GridViewEditRow input[type=text] {
            width: 150px;
        }

        .form-group > span {
            color: aliceblue;
        }
    </style>

    <link href="layout.css" rel="stylesheet" type="text/css" />
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/jquery-ui.css" rel="stylesheet" />
    <script src="Scripts/jquery-1.10.2.js"></script>
    <script src="Scripts/jquery-ui.js"></script>
    <script>
        function toggle() {
            var dropdwonlist = document.getElementById("Dropdownlist");
            if (dropdwonlist.style.display == 'none' || dropdwonlist.style.display == '') {
                dropdwonlist.style.display = 'block';
            }
            else {
                dropdwonlist.style.display = 'none';

            }
        }

        $(document).ready(function () {

            $(function () {

                $.datepicker.regional['zh-TW'] = {
                    clearText: '清除', clearStatus: '清除已選日期',
                    closeText: '關閉', closeStatus: '取消選擇',
                    prevText: '<上一月', prevStatus: '顯示上個月',
                    nextText: '下一月>', nextStatus: '顯示下個月',
                    currentText: '今天', currentStatus: '顯示本月',
                    monthNames: ['一月', '二月', '三月', '四月', '五月', '六月',
                        '七月', '八月', '九月', '十月', '十一月', '十二月'],
                    monthNamesShort: ['一', '二', '三', '四', '五', '六',
                        '七', '八', '九', '十', '十一', '十二'],
                    monthStatus: '選擇月份', yearStatus: '選擇年份',
                    weekHeader: '周', weekStatus: '',
                    dayNames: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
                    dayNamesShort: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
                    dayNamesMin: ['日', '一', '二', '三', '四', '五', '六'],
                    dayStatus: '設定每周第一天', dateStatus: '選擇 m月 d日, DD',
                    dateFormat: 'yy-mm-dd', firstDay: 1,
                    initStatus: '請選擇日期', isRTL: false,
                    changeMonth: true, changeYear: true,
                    yearRange: '1980:+1'
                };
                $(".DatePicker").datepicker().attr('readonly', 'readonly');
                $.datepicker.setDefaults($.datepicker.regional['zh-TW']);

                $(".ReadOnly").attr('readonly', 'readonly');

                $(':input:visible').addClass('enterIndex');
                textboxes = $('.enterIndex');
                $(textboxes).bind('keydown', CheckforEnter);
            });
        });

        function CheckforEnter(event) {
            if (event.keyCode == 13 && $(this).attr('type') != 'button') {
                var i = $('.enterIndex').index($(this));
                var n = $('.enterIndex').length;
                if (i < n - 1) {
                    NextDOM($('.enterIndex'), i);
                }
                return false;
            }
        }

        function NextDOM(myjQueryObjects, counter) {
            if (myjQueryObjects.eq(counter + 1)[0].disabled) {
                NextDOM(myjQueryObjects, counter + 1);
            }
            else {
                myjQueryObjects.eq(counter + 1).trigger('focus');
            }
        }

    </script>
</head>
<script type="text/javascript" src="main.js"></script>
<body>
    <form id="form1" runat="server">
        <div class="container-fluid img-fluid">
            <asp:Image ID="img_Title" runat="server" ImageUrl="~/title1.png" />
            <p hidden>
                <asp:Label ID="PWD" runat="server"></asp:Label>
            </p>
        </div>

        <div id="NAV" class="container-fluid">
            <ul class="navbar-nav nav-fill w-100 d-inline-block">
                <li class="w-25"><a href="WebForm4.aspx">零件診斷</a></li>
                <li class="w-25" onclick="toggle()">
                    <a class="active">新增維修紀錄 <span class="caret"></span></a>
                    <ul id="Dropdownlist" style="display: none;">
                        <li><a href="WebForm2.aspx">新增單筆維修紀錄</a></li>
                        <li><a href="WebForm3.aspx">新增多筆維修紀錄</a></li>
                    </ul>
                </li>
                <li class="w-25"><a href="WebForm9.aspx">資料庫功能</a></li>
                <%--<li><a href="WebForm1.aspx">歷史分析</a></li>--%>
                <li class="w-25"><a onclick="inputPassword()">管理頁面</a></li>
            </ul>
        </div>
        <div class="container-fluid auto-style3">
            <div class="row">
                <div class="col-12 col-lg-6 col-xl-4 form-group">
                    <asp:Label runat="server" ForeColor="Orange" Font-Names="Times New Roman" Text="*"></asp:Label>
                    <asp:Label runat="server" Text="工 單 號 碼："></asp:Label>
                    <asp:TextBox ID="WorkorderNum" runat="server" class="form-control"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_tb_WorkorderNum" runat="server" ErrorMessage="工單號碼為必填" Display="Dynamic" Font-Size="X-Large" SetFocusOnError="True" ControlToValidate="WorkorderNum" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="rev_tb_WorkorderNum" runat="server" ErrorMessage="錯誤 !! 工單號碼格式錯誤 請重新輸入" Display="Dynamic" Font-Size="X-Large" SetFocusOnError="True" ControlToValidate="WorkorderNum" ForeColor="Red" ValidationExpression="\d*"></asp:RegularExpressionValidator>
                </div>
                <div class="col-12 col-lg-6 col-xl-4 form-group">
                    <asp:Label runat="server" ForeColor="Orange" Font-Names="Times New Roman" Text="*"></asp:Label>
                    <asp:Label ID="lbl_UUT_SN" runat="server" Text="B E C 序 號："></asp:Label>
                    <asp:TextBox ID="SerialNum" runat="server" class="form-control" AutoPostBack="True" OnTextChanged="SerialNum_TextChanged"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_tb_SerialNum" runat="server" ErrorMessage="序號為必填" Display="Dynamic" Font-Size="X-Large" SetFocusOnError="True" ControlToValidate="SerialNum" ForeColor="Red"></asp:RequiredFieldValidator>
                </div>
                <div class="col-12 col-lg-6 col-xl-4 form-group">
                    <asp:Label runat="server" ForeColor="Orange" Font-Names="Times New Roman" Text="*"></asp:Label>
                    <asp:Label ID="lbl_UUT_PN" runat="server" Text="B E C 件 號："></asp:Label>
                    <asp:TextBox ID="PartNum" runat="server" class="form-control" placeholder="範例：1234567-89"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="rev_tb_PartNum" runat="server" ErrorMessage="錯誤 !! 件號格式錯誤 請重新輸入" Display="Dynamic" Font-Size="X-Large" SetFocusOnError="True" ControlToValidate="PartNum" ForeColor="Red" ValidationExpression="\d{7}-\d{2}"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="rfv_tb_PartNum" runat="server" ErrorMessage="件號為必填" Display="Dynamic" Font-Size="X-Large" SetFocusOnError="True" ControlToValidate="PartNum" ForeColor="Red"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="row">
                <div class="col-12 form-group">
                    <asp:Label runat="server" Text="掛 簽 缺 點 描 述："></asp:Label>
                    <asp:TextBox ID="Description" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                </div>
            </div>
            <div class="row">
                <div class="col-12 form-group">
                    <asp:Label runat="server" Text="檢 測 失 效 紀 錄："></asp:Label>
                    <asp:TextBox ID="FailedRecord" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                </div>
            </div>
            <div class="row">
                <div class="col-12 form-group">
                    <asp:Label runat="server" Text="CHECKLIST補充說明："></asp:Label>
                    <asp:TextBox ID="Checklist" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-xl-6 form-group">
                    <asp:Label ID="lbl_LastShipDate" runat="server" Text="上次出貨日期："></asp:Label>
                    <asp:TextBox ID="LastShipDate" runat="server" class="form-control DatePicker"></asp:TextBox>
                </div>
                <div class="col-lg-12 col-xl-6 form-group">
                    <asp:Label ID="lbl_CompleteDate" runat="server" Text="完工日期："></asp:Label>
                    <asp:TextBox ID="CompleteDate" runat="server" class="form-control DatePicker"></asp:TextBox>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-xl-6 form-group">
                    <asp:Label runat="server" ForeColor="Orange" Font-Names="Times New Roman" Text="*"></asp:Label>
                    <asp:Label runat="server" Text="接 收 日 期："></asp:Label>
                    <asp:TextBox ID="ReceiveDate" runat="server" class="form-control DatePicker" ClientIDMode="Static"></asp:TextBox>
                </div>
                <div class="col-lg-12 col-xl-6 form-group">
                    <asp:Label runat="server" ForeColor="Orange" Font-Names="Times New Roman" Text="*"></asp:Label>
                    <asp:Label runat="server" Text="出 貨 日 期："></asp:Label>
                    <asp:TextBox ID="ShipDate" runat="server" class="form-control DatePicker" ClientIDMode="Static"></asp:TextBox>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-6 col-xl-4 form-group">
                    <asp:Label runat="server" ForeColor="Orange" Font-Names="Times New Roman" Text="*"></asp:Label>
                    <asp:Label runat="server" Text="零 件 件 號："></asp:Label>
                    <asp:TextBox ID="ComponentNum" runat="server" class="form-control"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_tb_ComponentNum" runat="server" ErrorMessage="零件件號為必填" Display="Dynamic" Font-Size="X-Large" SetFocusOnError="True" ControlToValidate="ComponentNum" ForeColor="Red"></asp:RequiredFieldValidator>
                </div>

                <div class="col-12 col-lg-6 col-xl-4 form-group">
                    <asp:Label runat="server" ForeColor="Orange" Font-Names="Times New Roman" Text="*"></asp:Label>
                    <asp:Label runat="server" Text="零 件 中 文 名 稱："></asp:Label>
                    <asp:TextBox ID="ChineseName" runat="server" class="form-control"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_tb_ChineseName" runat="server" ErrorMessage="零件中文名稱為必填" Display="Dynamic" Font-Size="X-Large" SetFocusOnError="True" ControlToValidate="ChineseName" ForeColor="Red"></asp:RequiredFieldValidator>
                </div>
                <div class="col-12 col-lg-6 col-xl-4 form-group">
                    <asp:Label runat="server" ForeColor="Orange" Font-Names="Times New Roman" Text="*"></asp:Label>
                    <asp:Label runat="server" Text="數 量："></asp:Label>
                    <asp:TextBox ID="Amount" runat="server" class="form-control"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="rev_tb_Amount" runat="server" ErrorMessage="錯誤 !! 零件數量格式錯誤 請重新輸入" Display="Dynamic" Font-Size="X-Large" SetFocusOnError="True" ControlToValidate="Amount" ForeColor="Red" ValidationExpression="\d*"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="rfv_tb_Amount" runat="server" ErrorMessage="零件數量為必填" Display="Dynamic" Font-Size="X-Large" SetFocusOnError="True" ControlToValidate="Amount" ForeColor="Red"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="row">
                <div class="col-12 form-group">
                    <asp:Label ID="AddError" runat="server" ForeColor="#FFA500" Visible="false"></asp:Label>
                </div>
                <div class="col-12 form-group">
                    <asp:Label runat="server" ForeColor="Orange" Font-Names="Times New Roman" Text="*"></asp:Label>
                    <asp:Label runat="server" ForeColor="Orange" Text="為必填項目"></asp:Label>
                </div>
            </div>
            <div class="row">
                <div class="col-12 form-group">
                    <asp:Button ID="Add" runat="server" CssClass="btn btn-primary" Text="新增" OnClick="Add_Click" Height="60px" />
                </div>
            </div>
        </div>
        <asp:GridView ID="GridView1" runat="server" Width="2400px" AutoGenerateColumns="False" DataKeyNames="counter" DataSourceID="SqlDataSource1">
            <EditRowStyle BackColor="PaleGoldenRod " CssClass="GridViewEditRow" />
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:TemplateField ShowHeader="False">
                    <EditItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="更新" OnClientClick="return confirm('確認要修改嗎？');"></asp:LinkButton>
                        &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="取消"></asp:LinkButton>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit" Text="編輯"></asp:LinkButton>
                        &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Delete" Text="刪除" OnClientClick="return confirm('確認要刪除嗎？');"></asp:LinkButton>
                    </ItemTemplate>
                    <ItemStyle Width="80px" />
                </asp:TemplateField>
                <asp:BoundField DataField="Work_number" HeaderText="工單號碼" SortExpression="Work_number">
                    <ItemStyle Width="30px" />
                </asp:BoundField>
                <asp:BoundField DataField="BEC_Serial_number" HeaderText="序號" SortExpression="BEC_Serial_number">
                    <ItemStyle Width="50px" />
                </asp:BoundField>
                <asp:BoundField DataField="BEC_Part_Number" HeaderText="件號" SortExpression="BEC_Part_Number">
                    <ItemStyle Width="40px" />
                </asp:BoundField>
                <asp:BoundField DataField="BEC_completion_Date" HeaderText="完工日期" SortExpression="BEC_completion_Date">
                    <ItemStyle Width="80px" />
                </asp:BoundField>
                <asp:BoundField DataField="Receipt_Date" HeaderText="接收日期" SortExpression="Receipt_Date">
                    <ItemStyle Width="80px" />
                </asp:BoundField>
                <asp:BoundField DataField="Ship_Date" HeaderText="出貨日期" SortExpression="Ship_Date">
                    <ItemStyle Width="80px" />
                </asp:BoundField>
                <asp:BoundField DataField="Fault_Description" HeaderText="缺點描述" SortExpression="Fault_Description">
                    <ItemStyle Width="100px" />
                </asp:BoundField>
                <asp:BoundField DataField="CHECKLIST_Description" HeaderText="CHECKLIST描述" SortExpression="CHECKLIST_Description">
                    <ItemStyle Width="100px" />
                </asp:BoundField>
                <asp:BoundField DataField="Detect_Failure_Record" HeaderText="失效檢測紀錄" SortExpression="Detect_Failure_Record">
                    <ItemStyle Width="100px" />
                </asp:BoundField>
                <asp:BoundField DataField="Components_number" HeaderText="零件件號" SortExpression="Components_number">
                    <ItemStyle Width="50px" />
                </asp:BoundField>
                <asp:BoundField DataField="Components_name" HeaderText="零件名稱" SortExpression="Components_name">
                    <ItemStyle Width="50px" />
                </asp:BoundField>
                <asp:BoundField DataField="number" HeaderText="數量" SortExpression="number">
                    <ItemStyle Width="20px" HorizontalAlign="Right" />
                </asp:BoundField>
            </Columns>
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />

        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" DeleteCommand="DELETE FROM [SingleTempData] WHERE [counter] = @original_counter AND (([Components_name] = @original_Components_name) OR ([Components_name] IS NULL AND @original_Components_name IS NULL)) AND (([number] = @original_number) OR ([number] IS NULL AND @original_number IS NULL))"
            InsertCommand="INSERT INTO [SingleTempData] ([Work_number], [BEC_Serial_number], [BEC_Part_Number], [BEC_completion_Date], [Receipt_Date], [Ship_Date], [Fault_Description], [CHECKLIST_Description], [Detect_Failure_Record], [Components_number], [Components_name], [number], [counter]) VALUES (@Work_number, @BEC_Serial_number, @BEC_Part_Number, @BEC_completion_Date, @Receipt_Date, @Ship_Date, @Fault_Description, @CHECKLIST_Description, @Detect_Failure_Record, @Components_number, @Components_name, @number, @counter)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [SingleTempData]"
            UpdateCommand="UPDATE [SingleTempData] SET [Work_number] = @Work_number, [BEC_Serial_number] = @BEC_Serial_number, [BEC_Part_Number] = @BEC_Part_Number, [BEC_completion_Date] = @BEC_completion_Date, [Receipt_Date] = @Receipt_Date, [Ship_Date] = @Ship_Date, [Fault_Description] = @Fault_Description, [CHECKLIST_Description] = @CHECKLIST_Description, [Detect_Failure_Record] = @Detect_Failure_Record, [Components_number] = @Components_number, [Components_name] = @Components_name, [number] = @number WHERE [counter] = @original_counter AND (([Components_number] = @original_Components_number) OR ([Components_number] IS NULL AND @original_Components_number IS NULL)) AND (([Components_name] = @original_Components_name) OR ([Components_name] IS NULL AND @original_Components_name IS NULL)) AND (([number] = @original_number) OR ([number] IS NULL AND @original_number IS NULL))">
            <DeleteParameters>
                <asp:Parameter Name="original_counter" Type="String" />
                <asp:Parameter Name="original_Work_number" Type="Int32" />
                <asp:Parameter Name="original_BEC_Serial_number" Type="String" />
                <asp:Parameter Name="original_BEC_Part_Number" Type="String" />
                <asp:Parameter Name="original_BEC_completion_Date" Type="String" />
                <asp:Parameter Name="original_Receipt_Date" Type="String" />
                <asp:Parameter Name="original_Ship_Date" Type="String" />
                <asp:Parameter Name="original_Fault_Description" Type="String" />
                <asp:Parameter Name="original_CHECKLIST_Description" Type="String" />
                <asp:Parameter Name="original_Detect_Failure_Record" Type="String" />
                <asp:Parameter Name="original_Components_number" Type="String" />
                <asp:Parameter Name="original_Components_name" Type="String" />
                <asp:Parameter Name="original_number" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Work_number" Type="Int32" />
                <asp:Parameter Name="BEC_Serial_number" Type="String" />
                <asp:Parameter Name="BEC_Part_Number" Type="String" />
                <asp:Parameter Name="BEC_completion_Date" Type="String" />
                <asp:Parameter Name="Receipt_Date" Type="String" />
                <asp:Parameter Name="Ship_Date" Type="String" />
                <asp:Parameter Name="Fault_Description" Type="String" />
                <asp:Parameter Name="CHECKLIST_Description" Type="String" />
                <asp:Parameter Name="Detect_Failure_Record" Type="String" />
                <asp:Parameter Name="Components_number" Type="String" />
                <asp:Parameter Name="Components_name" Type="String" />
                <asp:Parameter Name="number" Type="Int32" />
                <asp:Parameter Name="counter" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="Work_number" Type="Int32" />
                <asp:Parameter Name="BEC_Serial_number" Type="String" />
                <asp:Parameter Name="BEC_Part_Number" Type="String" />
                <asp:Parameter Name="BEC_completion_Date" Type="String" />
                <asp:Parameter Name="Receipt_Date" Type="String" />
                <asp:Parameter Name="Ship_Date" Type="String" />
                <asp:Parameter Name="Fault_Description" Type="String" />
                <asp:Parameter Name="CHECKLIST_Description" Type="String" />
                <asp:Parameter Name="Detect_Failure_Record" Type="String" />
                <asp:Parameter Name="Components_number" Type="String" />
                <asp:Parameter Name="Components_name" Type="String" />
                <asp:Parameter Name="number" Type="Int32" />
                <asp:Parameter Name="original_counter" Type="String" />
                <asp:Parameter Name="original_Work_number" Type="Int32" />
                <asp:Parameter Name="original_BEC_Serial_number" Type="String" />
                <asp:Parameter Name="original_BEC_Part_Number" Type="String" />
                <asp:Parameter Name="original_BEC_completion_Date" Type="String" />
                <asp:Parameter Name="original_Receipt_Date" Type="String" />
                <asp:Parameter Name="original_Ship_Date" Type="String" />
                <asp:Parameter Name="original_Fault_Description" Type="String" />
                <asp:Parameter Name="original_CHECKLIST_Description" Type="String" />
                <asp:Parameter Name="original_Detect_Failure_Record" Type="String" />
                <asp:Parameter Name="original_Components_number" Type="String" />
                <asp:Parameter Name="original_Components_name" Type="String" />
                <asp:Parameter Name="original_number" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <p></p>
        <div align="center" style="color: black">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="SendBtn" runat="server" Height="65px" OnClick="SendBtn_Click" Text="送出" Width="95px" Font-Size="X-Large" Visible="False" />
        </div>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<asp:TextBox ID="error3" runat="server" BorderStyle="None" ForeColor="#FFA500" Font-Size="xlarge" Height="30px" Visible="False" Width="543px"></asp:TextBox>
        </p>
        </div>

    </form>

</body>
</html>
