﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm4.aspx.cs" Inherits="BigData.WebForm4" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>零件維修大數據統計系統</title>
    <style type="text/css">
        .auto-style1 {
            text-align: center;
            font-family: DFKai-sb,"標楷體";
        }

        .auto-style2 {
            font-family: DFKai-sb,"標楷體";
        }

        .auto-style3 {
            font-size: 34px;
            font-family: DFKai-sb,"標楷體";
            color: aliceblue;
        }

        .auto-style4 {
            text-align: left;
            font-size: 34px;
            font-family: DFKai-sb,"標楷體";
            color: aliceblue;
        }
    </style>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="layout.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="main.js"></script>
</head>
<body>
    <form id="form1" runat="server" style="background-color: transparent">
        <div class="container-fluid img-fluid">
            <asp:Image ID="img_Title" runat="server" ImageUrl="~/title1.png" />
            <p hidden>
                <asp:Label ID="PWD" runat="server"></asp:Label>
            </p>
        </div>
        <div id="NAV" class="container-fluid">
            <ul class="navbar-nav nav-fill w-100 d-inline-block">
                <li class="w-25"><a class="active" href="WebForm4.aspx">零件診斷</a></li>
                <li class="w-25" onclick="toggle()">
                    <a>新增維修紀錄 <span class="caret"></span></a>
                    <ul id="Dropdownlist" style="display: none;">
                        <li><a href="WebForm2.aspx">新增單筆維修紀錄</a></li>
                        <li><a href="WebForm3.aspx">新增多筆維修紀錄</a></li>
                    </ul>
                </li>
                <li class="w-25"><a href="WebForm9.aspx">資料庫功能</a></li>
                <%--<li><a href="WebForm1.aspx">歷史分析</a></li>--%>
                <li class="w-25"><a onclick="inputPassword()">管理頁面</a></li>
            </ul>
        </div>

        <div class="container-fluid">
            <div class="auto-style2 form-group">
                <asp:Label ID="lbl_Title_UUT_SN" runat="server" CssClass="auto-style3">輸入序號：</asp:Label>
                <asp:DropDownList ID="ddl_UUT_SN" runat="server" CssClass="custom-select custom-select-lg " Height="60px" Width="220px" DataSourceID="SqlDataSource_BEC_SN" DataTextField="BEC_Serial_number" DataValueField="BEC_Serial_number" Font-Size="XX-Large">
                </asp:DropDownList>
                <asp:Button ID="btn_Confirm" CssClass="btn btn-info" runat="server" Height="60px" OnClick="BECBtn_Click" Text="確認" Width="120px" Font-Size="XX-Large" />
            </div>
            <div class="auto-style2 form-group">
                <asp:Label ID="lbl_Title_ShowData" runat="server" CssClass="auto-style3" Visible="false">失效零件資訊</asp:Label>
                <asp:GridView ID="gv_ShowData" CssClass="table" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%"
                    HeaderStyle-HorizontalAlign="Center"
                    RowStyle-HorizontalAlign="Center">
                    <AlternatingRowStyle BackColor="White" />
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
            </div>
            <div class="form-group">
                <p class="auto-style2" style="text-align: center">
                    <asp:Button ID="GenerateReportBtn" CssClass="btn btn-success" runat="server" Text="產生報表" Height="60px" OnClientClick="window.open(&quot;WebForm6.aspx&quot;, &quot;_blank&quot;);" Width="220px" Visible="False" Font-Size="XX-Large" />
                    &nbsp;
                </p>
            </div>
        </div>
    </form>
    <asp:SqlDataSource ID="SqlDataSource_BEC_SN" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString_BigData %>" SelectCommand="SELECT DISTINCT [BEC_Serial_number] FROM [final_new]"></asp:SqlDataSource>
</body>
</html>
