﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace BigData
{
    public partial class WebForm9 : System.Web.UI.Page
    {
        // 資料庫功能載入頁面
        protected void Page_Load(object sender, EventArgs e)
        {
            // 如果使用者的登入狀態是空的，表示閒置太久或沒有登入
            if (Session["isLogin"] == null)
            {
                // 轉至登入頁面
                //Response.Redirect("Login.aspx");
                Response.Redirect("Info.aspx?info=3");
            }

            // 連接資料庫
            SqlConnection connection = OpenSQLConnection();
            // 取得設定中的密碼
            Getpwd(connection);

            // 取得總資料筆數，總零件件數，並計算訓練所需大約時間
            int dataCount = GetDataCount();
            int componentCount = GetComponentCount();
            int needTime = dataCount * componentCount / 12500;
            TimeSpan.Text = "總資料筆數: " + dataCount + " 總零件件數: " + componentCount + "<br />約需時間: " + needTime + "~" + (needTime + 1) + " 分鐘";

            string sUUT_Name = Session["WorkStation"].ToString();
            GridView1.Columns[3].HeaderText = sUUT_Name + "序號";
            GridView1.Columns[4].HeaderText = sUUT_Name + "件號";
            GridView1.Columns[5].HeaderText = sUUT_Name + "完工";
        }

        // 開啟資料庫連線
        private static SqlConnection OpenSQLConnection()
        {
            String conn = "Server=localhost;User id=LoginAIDC;Password=aidc2019;database=test;";
            SqlConnection connection = new SqlConnection(conn);
            connection.Open();
            return connection;
        }

        // 取得設定中的密碼
        private void Getpwd(SqlConnection connection)
        {
            var command = "SELECT password FROM Settings";
            SqlCommand pwdcmd = new SqlCommand(command, connection);
            SqlDataReader reader = pwdcmd.ExecuteReader();
            while (reader.Read())
            {
                PWD.Text = reader.GetString(0);
            }
            connection.Close();
        }

        // 取得總資料筆數
        private static int GetDataCount()
        {
            int dataCount = 0;
            SqlConnection connection = OpenSQLConnection();
            var command = "select count(*) from final_new";
            SqlCommand cmd = new SqlCommand(command, connection);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                dataCount = reader.GetInt32(0);
            }
            connection.Close();
            return dataCount;
        }

        // 取得零件件數
        private static int GetComponentCount()
        {
            int componentCount = 0;
            SqlConnection connection = OpenSQLConnection();
            var command = "select count(distinct(components_number)) from final_new";
            SqlCommand cmd = new SqlCommand(command, connection);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                componentCount = reader.GetInt32(0);
            }
            connection.Close();
            return componentCount;
        }

        // 執行Rscript執行檔
        private static string RunRScript(string rpath, string scriptpath, string v)
        {
            try
            {
                // 程序需要的資訊
                var info = new ProcessStartInfo
                {
                    FileName = rpath,
                    WorkingDirectory = Path.GetDirectoryName(scriptpath),
                    Arguments = scriptpath + " " + v,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true,
                    UseShellExecute = false
                };
                // 新增程序以執行外部程式R
                using (var proc = new Process { StartInfo = info })
                {
                    // 開始程序
                    proc.Start();
                    // 回傳R程式output
                    return proc.StandardOutput.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return string.Empty;
        }

        protected void TrainBtn_Click(object sender, EventArgs e)
        {
            // 取得SQL連線
            String conn = "Server=localhost;User id=LoginAIDC;Password=aidc2019;database=test;";
            SqlConnection connection = new SqlConnection(conn);

            // 產生BEC工單資料的csv檔
            GenerateBECWorkOrderFile(connection);

            // 產生BEC提料清單的csv檔
            GenerateBECComponentFile(connection);

            // 執行模型訓練，並顯示訓練的時間
            var rpath = @"C:\Program Files\R\R-3.4.4\bin\Rscript.exe";
            var scriptpath = @"C:\R\LR\model_train_v2.R";
            // 執行R程式
            var r_output = RunRScript(rpath, scriptpath, string.Empty);
            // 訓練所有板件模型
            TrainAllBoardModel();
            // 顯示更新完成訊息
            string msg = "模型更新完成!";
            string Alertmsg = "<script language = JavaScript > alert('" + msg + "'); </script>";
            Response.Write(Alertmsg);
        }

        // 訓練所有板件模型
        private static void TrainAllBoardModel()
        {
            var rpath = @"C:\Program Files\R\R-3.4.4\bin\Rscript.exe";
            List<string> boardList = new List<string>() { "A1", "A2", "A3", "Transducer" };
            foreach (string board in boardList)
            {
                var scriptpath = @"C:\R\LR\" + board + "_train.R";
                var output = RunRScript(rpath, scriptpath, string.Empty);
            }
        }

        // 產生BEC提料清單的csv檔
        private static void GenerateBECComponentFile(SqlConnection connection)
        {
            connection.Open();
            String pickup = "SELECT BEC_Serial_number, Work_number, Components_number FROM final_new";
            SqlCommand pickup_cmd = new SqlCommand(pickup, connection);
            SqlDataReader pickup_reader = pickup_cmd.ExecuteReader();
            using (DataTable dt = new DataTable())
            {
                dt.Load(pickup_reader);
                Console.WriteLine(dt.Rows.Count);
                StreamWriter CsvfileWriter = new StreamWriter(@"C:\R\LR\pickup.csv");
                // 寫入csv檔
                using (CsvfileWriter)
                {
                    CsvfileWriter.WriteLine(string.Join(",", dt.Columns.Cast<DataColumn>().Select(csvfile => csvfile.ColumnName)));
                    foreach (DataRow row in dt.Rows)
                    {
                        CsvfileWriter.WriteLine(string.Join(",", row.ItemArray));
                    }
                }
            }
            connection.Close();
        }

        // 產生BEC工單資料的csv檔
        private static void GenerateBECWorkOrderFile(SqlConnection connection)
        {
            connection.Open();
            String final = "SELECT BEC_Serial_number, Work_number, BEC_Part_Number, Receipt_Date, Ship_Date, BEC_completion_Date FROM final_new";
            SqlCommand final_cmd = new SqlCommand(final, connection);
            SqlDataReader final_reader = final_cmd.ExecuteReader();
            using (DataTable dt = new DataTable())
            {
                dt.Load(final_reader);
                Console.WriteLine(dt.Rows.Count);
                StreamWriter CsvfileWriter = new StreamWriter(@"C:\R\LR\merge.csv");
                // 寫入csv檔
                using (CsvfileWriter)
                {
                    CsvfileWriter.WriteLine(string.Join(",", dt.Columns.Cast<DataColumn>().Select(csvfile => csvfile.ColumnName)));
                    foreach (DataRow row in dt.Rows)
                    {
                        CsvfileWriter.WriteLine(string.Join(",", row.ItemArray));
                    }
                }
            }
            connection.Close();
        }
    }
}