﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace BigData
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        // 新增多筆維修紀錄載入頁面
        protected void Page_Load(object sender, EventArgs e)
        {
            // 如果使用者的登入狀態是空的，表示閒置太久或沒有登入
            if (Session["isLogin"] == null)
            {
                // 轉至登入頁面
                Response.Redirect("Login.aspx");
            }
            // 連接資料庫
            SqlConnection connection = OpenSQLConnection();
            // 取得設定中的密碼
            Getpwd(connection);
            // 如果是第一次載入
            if (!IsPostBack)
            {
                // 清除TempData的資料表(把新增多筆的清空)
                TruncateTempDataTable(connection);
            }
        }

        // 觸發預覽按鈕
        protected void PreviewBtn_Click(object sender, EventArgs e)
        {
            // 連接資料庫
            SqlConnection connection = OpenSQLConnection();
            // 清除TempData的資料表(把新增多筆的清空)
            TruncateTempDataTable(connection);

            // 取得檔案名稱
            string fileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
            // 取得檔案的附檔名
            string extension = Path.GetExtension(fileName).ToLowerInvariant();

            // 規範只能允許上傳csv檔
            List<string> allowedExtension = new List<string> { ".csv" };
            // 如果上傳的不適csv檔案
            if(allowedExtension.IndexOf(extension) == -1)
            {
                // 跳出提醒訊息
                var msg = "請上傳csv檔";
                string Alertmsg = "<script language = JavaScript > alert('" + msg + "'); </script>";
                Response.Write(Alertmsg);
                return;
            }

            // 儲存該csv檔案再Files資料夾內
            string csvPath = Server.MapPath("~/Files/") + fileName;
            FileUpload1.SaveAs(csvPath);

            // 創建資料表以存放上傳的檔案內容
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[12] { new DataColumn("Work_number", typeof(string)),
            new DataColumn("BEC_Serial_number", typeof(string)),
            new DataColumn("BEC_Part_Number",typeof(string)),
            new DataColumn("BEC_completion_Date_Number",typeof(string)),
            new DataColumn("Receipt_Date",typeof(string)),
            new DataColumn("Ship_Date",typeof(string)),
            new DataColumn("Fault_Description",typeof(string)),
            new DataColumn("CHECKLIST_Description",typeof(string)),
            new DataColumn("Detect_Failure_Record",typeof(string)),
            new DataColumn("Components_number",typeof(string)),
            new DataColumn("Components_name",typeof(string)),
            new DataColumn("number",typeof(string))
            });


            //讀取csv檔案
            string csvData = File.ReadAllText(csvPath);
            foreach (string row in csvData.Split('\n'))
            {
                if (!string.IsNullOrEmpty(row))
                {
                    dt.Rows.Add();
                    int i = 0;
                    foreach (string cell in row.Split(','))
                    {
                        dt.Rows[dt.Rows.Count - 1][i] = cell;
                        i++;
                    }
                }
            }

            // 設定要連線的資料庫及登入的帳密
            string consString = "Server=localhost;User id=LoginAIDC;Password=aidc2019;database=test;";
            // 建立連線
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                {
                    //把上傳的檔案存到資料庫的資料表TempData
                    sqlBulkCopy.DestinationTableName = "dbo." + "TempData";
                    con.Open();
                    sqlBulkCopy.WriteToServer(dt);
                    con.Close();
                }
            }
            // 設置GridView1的資料來源
            GridView1.DataSourceID = "SqlDataSource1";
            // 顯示送出按鈕
            SendBtn.Visible = true;
        }

        // 清除TempData的資料表(把新增多筆的清空)
        private static void TruncateTempDataTable(SqlConnection connection)
        {
            // 連接資料庫
            connection.Open();
            // 清空資料表SQL指令
            string delcommand = "TRUNCATE TABLE TempData";
            // 新增指令
            SqlCommand delcmd = new SqlCommand(delcommand, connection);
            // 執行指令
            delcmd.ExecuteNonQuery();
            // 關閉資料庫連線
            connection.Close();
        }

        // 建立資料庫連線
        private static SqlConnection OpenSQLConnection()
        {
            // 設定要連線的資料庫及登入的帳密
            String conn = "Server=localhost;User id=LoginAIDC;Password=aidc2019;database=test;";
            // 建立連線
            SqlConnection connection = new SqlConnection(conn);
            // 回傳該連線
            return connection;
        }

        // 取得設定中的密碼
        private void Getpwd(SqlConnection connection)
        {
            // 連接資料庫
            connection.Open();
            // 搜尋密碼SQL指令
            var command = "SELECT password FROM Settings";
            // 新增SQL指令
            SqlCommand pwdcmd = new SqlCommand(command, connection);
            // 執行SQL指令
            SqlDataReader reader = pwdcmd.ExecuteReader();
            // 讀取指令結果
            while (reader.Read())
            {
                // 存取密碼
                PWD.Text = reader.GetString(0);
            }
            // 關閉資料庫連線
            connection.Close();
        }

        // 觸發送出按鈕
        protected void SendBtn_Click(object sender, EventArgs e)
        {
            int line = 0, column = 0;
            int temp = 0, firstTemp = 0;
            try
            {
                // 連接資料庫
                SqlConnection connection = OpenSQLConnection();
                connection.Open();
                // 取得目前資料筆數SQL指令
                String cmdText1 = "SELECT DISTINCT counter FROM final_new";
                // 新增指令
                SqlCommand cmd1 = new SqlCommand(cmdText1, connection);
                // 執行指令
                SqlDataReader reader1 = cmd1.ExecuteReader();
                // 讀去總資料筆數
                while (reader1.Read())
                {
                    temp = reader1.GetInt16(0);
                    firstTemp = temp + 1;
                }
                // 關閉資料庫連線
                connection.Close();

                // 檢查新增多筆維修紀錄的所有資料格式，正確就匯入資料庫
                // 連接資料庫
                connection.Open();
                // 選取所有新增多筆的資料
                String cmdText2 = "SELECT * FROM TempData";
                // 新增指令
                SqlCommand cmd2 = new SqlCommand(cmdText2, connection);
                // 執行指令
                SqlDataReader reader2 = cmd2.ExecuteReader();
                while (reader2.Read())
                {
                    // 檢查資料格式
                    line += 1;
                    column = 1;
                    int workorderNum = Int32.Parse(reader2.GetString(0));
                    column = 12;
                    int amount = reader2.GetInt32(11);
                    column = 4;
                    DateTime completeDate = Convert.ToDateTime(reader2.GetString(3));
                    column = 5;
                    DateTime receiveDate = Convert.ToDateTime(reader2.GetString(4));
                    column = 6;
                    DateTime shipDate = Convert.ToDateTime(reader2.GetString(5));

                    // 連接資料庫，並匯入資料庫
                    SqlConnection connection2 = OpenSQLConnection();
                    connection2.Open();
                    String cmdText = " INSERT INTO final_new (Work_number, BEC_Serial_number, BEC_Part_Number, BEC_completion_Date, Receipt_Date, Ship_Date, Fault_Description, CHECKLIST_Description, Detect_Failure_Record, Components_number, Components_name, number, counter) VALUES ( '" + reader2.GetString(0) + "', '" + reader2.GetString(1) + "', '" + reader2.GetString(2) + "', '" + reader2.GetString(3) + "', '" + reader2.GetString(4) + "', '" + reader2.GetString(5) + "', N'" + reader2.GetString(6) + "', N'" + reader2.GetString(7) + "', N'" + reader2.GetString(8) + "', '" + reader2.GetString(9) + "', N'" + reader2.GetString(10) + "', '" + reader2.GetInt32
                    (11) + "', '" + (temp + 1) + "');";
                    SqlCommand cmd = new SqlCommand(cmdText, connection2);
                    cmd.ExecuteNonQuery();
                    connection2.Close();
                    temp = temp + 1;
                }
                connection.Close();
                // 跳出送出成功訊息
                Response.Write("<script language=JavaScript> alert('送出成功'); </script>");

                // 清除TempData的資料表(把新增多筆的清空)
                SqlConnection connection4 = OpenSQLConnection();
                TruncateTempDataTable(connection4);

                // 綁定GridView1的資料來源
                GridView1.DataSourceID = "SqlDataSource1";
            }// 若檢查後資料格式有誤
            catch
            {
                // 跳出提醒訊息
                string msg = "送出失敗 請檢查第 " + line + " 行第 "+column+ " 欄資料格式是否正確";
                string Errormsg = "<script language = JavaScript > alert('" + msg + "'); </script>";
                Response.Write(Errormsg);

                // 清除TempData的資料表(把新增多筆的清空)
                SqlConnection connection = OpenSQLConnection();
                TruncateTempDataTable(connection);
                // 當欲匯入的資料表格式錯誤時，刪除匯入的資料
                DeleteHistoryData(firstTemp, temp);
            }

        }

        // 當欲匯入的資料表格式錯誤時，刪除已匯入的資料
        private void DeleteHistoryData(int firstTemp, int temp)
        {
            // 連接資料庫
            SqlConnection connection = OpenSQLConnection();
            // 開啟資料庫連線
            connection.Open();
            // 刪除資料指令
            string command = "DELETE FROM [dbo].[final_new] WHERE [counter] BETWEEN " + firstTemp + " AND " + temp;
            // 新增指令
            SqlCommand cmd = new SqlCommand(command, connection);
            // 執行指令
            cmd.ExecuteNonQuery();
            // 關閉資料庫連線
            connection.Close();
        }
    }
}