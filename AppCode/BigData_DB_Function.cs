﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BigData.AppCode
{
    public class BigData_DB_Function : System.Web.UI.Page
    {
        /// <summary>
        /// 開啟資料庫連線
        /// </summary>
        /// <param name="ConnectionStr"></param>
        /// <returns></returns>
        private SqlConnection GetSqlConnection(string ConnectionStr)
        {
            SqlConnection Conn = new SqlConnection(ConnectionStr);
            try
            {
                Conn.Open();
                return Conn;
            }
            catch (Exception)
            {
                Conn.Close();
                return null;
            }
        }

        /// <summary>
        /// 將資料庫讀到的資料轉為DataTable
        /// </summary>
        /// <param name="SqlCmd">SQL語法，需先連線並且指定好Cmd與參數</param>
        /// <returns></returns>
        private DataTable GetDataTableFromDB(SqlCommand SqlCmd)
        {
            SqlDataAdapter da_BigData = new SqlDataAdapter(SqlCmd);
            DataTable tmp_DT = new DataTable("tmpDT");
            da_BigData.Fill(tmp_DT);
            return tmp_DT;
        }

        /// <summary>
        /// 檢查資料庫是否該User存在
        /// </summary>
        /// <param name="UserAccount">User帳號</param>
        /// <returns>true表示有該User，反之</returns>
        public bool IsUserExist(string UserAccount)
        {
            bool bResult = false;

            string base64Encoded = UserAccount;
            string base64Decoded;
            byte[] data = Convert.FromBase64String(base64Encoded);
            base64Decoded = System.Text.Encoding.ASCII.GetString(data);

            //用DB連線字串開啟DB連線
            SqlConnection Conn_BigData = GetSqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString_BigData"].ConnectionString);
            if (Conn_BigData != null)
            {
                //設定連線與SQL Command
                SqlCommand SqlCmd_BigData = new SqlCommand();
                SqlCmd_BigData.Connection = Conn_BigData;
                SqlCmd_BigData.CommandText = @"SELECT * FROM [UserInform] WHERE [Account] = @Account";
                SqlCmd_BigData.Parameters.AddWithValue("@Account", base64Decoded);

                DataTable dt_User = GetDataTableFromDB(SqlCmd_BigData);

                //讀取該用戶資料到Session
                if (dt_User.Rows.Count > 0)
                {
                    Session["AC"] = dt_User.Rows[0]["Account"].ToString();
                    Session["Username"] = dt_User.Rows[0]["Username"].ToString();
                    Session["IsAdmin"] = dt_User.Rows[0]["IsAdmin"].ToString();
                    bResult = true;
                }
            }
            Conn_BigData.Close();
            return bResult;
        }

        /// <summary>
        /// 比對User資料庫，帳號密碼是否符合
        /// </summary>
        /// <param name="UserAccount">User帳號</param>
        /// <param name="UserPassword">User密碼</param>
        /// <returns>true表示帳號密碼正確，反之</returns>
        public bool IsUserExist(string UserAccount, string UserPassword)
        {
            bool bResult = false;

            //用DB連線字串開啟DB連線
            SqlConnection Conn_BigData = GetSqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString_BigData"].ConnectionString);
            if (Conn_BigData != null)
            {
                //設定連線與SQL Command
                SqlCommand SqlCmd_BigData = new SqlCommand();
                SqlCmd_BigData.Connection = Conn_BigData;
                SqlCmd_BigData.CommandText = @"SELECT * FROM [UserInform] WHERE [Account] = @Account AND [Password] = @Password";
                SqlCmd_BigData.Parameters.AddWithValue("@Account", UserAccount);
                SqlCmd_BigData.Parameters.AddWithValue("@Password", UserPassword);

                DataTable dt_User = GetDataTableFromDB(SqlCmd_BigData);

                //讀取該用戶資料到Session
                if (dt_User.Rows.Count > 0)
                {
                    Session["AC"] = dt_User.Rows[0]["Account"].ToString();
                    Session["Username"] = dt_User.Rows[0]["Username"].ToString();
                    Session["IsAdmin"] = dt_User.Rows[0]["IsAdmin"].ToString();
                    bResult = true;
                }
            }
            Conn_BigData.Close();
            return bResult;
        }
    }
}