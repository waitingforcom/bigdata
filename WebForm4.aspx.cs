﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace BigData
{
    public partial class WebForm4 : System.Web.UI.Page
    {
        // 待測物診斷載入頁面
        protected void Page_Load(object sender, EventArgs e)
        {
            // 如果使用者的登入狀態是空的，表示閒置太久或沒有登入
            if (Session["isLogin"] == null)
            {
                // 轉至登入頁面
                Response.Write("<script language=JavaScript> alert('閒置太久或尚未登入!'); window.location='Info.aspx?info=1'; </script>");
            }
            // 連接資料庫
            SqlConnection connection = OpenSQLConnection();
            // 取得設定中的密碼
            GetPWD(connection);

            //根據UUT名稱顯示Label
            string sUUT_Name = Session["WorkStation"].ToString();
            lbl_Title_UUT_SN.Text = "請選擇" + sUUT_Name + "序號：";
        }

        // 觸發選取該待測物的按鈕
        protected void BECBtn_Click(object sender, EventArgs e)
        {
            // 連接資料庫
            SqlConnection connection = OpenSQLConnection();
            // 取得設定中生命週期與失效機率的設定值
            int defaultLifecycle;
            int defaultFailedRate;
            GetDefaultSettings(connection, out defaultLifecycle, out defaultFailedRate);
            // 清空report資料表
            TruncateReportTable(connection);

            // 報表第一列顯示該具待測物的生命週期和失效機率
            // 取得選取得待測物序號
            string sUUT_SerialNum = Server.HtmlEncode(ddl_UUT_SN.SelectedItem.Value.ToString());
            // 取得選取的待測物序號的所有失效過的零件，和上次出貨的日期SQL指令
            string command = "select Components_number, MAX(Ship_Date) from final_new where BEC_Serial_number like '%" + sUUT_SerialNum + "%' group by Components_number ";
            // 將待測物的失效機率和生命週期匯入report資料表
            InsertUUTInformToDB(connection, sUUT_SerialNum);

            // 取得該待測物上次出貨日期
            DateTime UUT_LastShipDate = new DateTime();
            UUT_LastShipDate = GetUUTLastShipDate(connection, sUUT_SerialNum, UUT_LastShipDate);
            // 取得已使用的天數
            int usedTime = (DateTime.Now - UUT_LastShipDate).Days;

            // 設定R執行檔位置
            var rpath = @"C:\Program Files\R\R-3.4.4\bin\Rscript.exe";
            // 設定要執行的R檔案
            var scriptpath = @"C:\R\LR\model_predict.R";
            // 執行R程式
            var r_output = RunRScript(rpath, scriptpath, sUUT_SerialNum, usedTime);
            // 取得R程式執行後的輸出並處理
            string output = ProcessROutput(r_output);
            // 處理output取得所有失效機率的清單
            List<float> failedrate_list = GetFailedRate(output);
            // 處理output取得前五名失效的零件
            List<string> component_list = GetComponent(output);

            // 設定要連線的資料庫及登入的帳密
            String conString = "Server=localhost;User id=LoginAIDC;Password=aidc2019;database=test;";
            // 新增資料庫連線
            using (SqlConnection sqlconnection = new SqlConnection(conString))
            {
                // 連接資料庫
                sqlconnection.Open();
                // 新增SQL指令
                SqlCommand cmd = new SqlCommand(command, sqlconnection);
                // 執行SQL指令
                SqlDataReader reader = cmd.ExecuteReader();
                // 逐行讀取資料
                while (reader.Read())
                {
                    string component = reader.GetString(0);
                    float failed_rate = 0;

                    // 依照讀取的零件給予其失效機率
                    for (int i = 0; i < component_list.Count; i++)
                    {
                        if (component.Equals(component_list[i]))
                        {
                            failed_rate = failedrate_list[i];
                        }
                    }

                    // 計算平均生命週期
                    float avgLifecycle = ComputeLifecycle(connection, component);
                    // 存取出貨日期
                    DateTime shipDate = DateTime.Parse(reader.GetString(1));
                    // 存取現在時間
                    DateTime now = DateTime.Now;
                    // 計算已使用的時間
                    TimeSpan usedDays = now - shipDate;
                    // 計算所剩的生命週期
                    avgLifecycle = avgLifecycle * (100 - failed_rate) / 100;
                    var remainDays = (avgLifecycle - usedDays.Days);

                    // report中僅顯示篩選後的資料筆數
                    if (remainDays <= defaultLifecycle || failed_rate >= defaultFailedRate)
                    {
                        // 將資料匯入report資料表以便顯示
                        connection.Open();
                        string cmdText3 = @"INSERT INTO report (BEC_Serial_number, Class, Components_number, lifecycle, lifecycle_Current, lifecycle_Average, Failure_percent) VALUES (@BEC_Serial_number, @Class, @Components_number, @lifecycle, @lifecycle_Current, @lifecycle_Average, @Failure_percent ); ";
                        SqlCommand cmd3 = new SqlCommand(cmdText3, connection);
                        cmd3.Parameters.AddWithValue("@BEC_Serial_number", sUUT_SerialNum);
                        cmd3.Parameters.AddWithValue("@Class", "零件");
                        cmd3.Parameters.AddWithValue("@Components_number", component);
                        cmd3.Parameters.AddWithValue("@lifecycle", remainDays.ToString("#0") + "/" + avgLifecycle.ToString("#0"));
                        cmd3.Parameters.AddWithValue("@lifecycle_Current", remainDays.ToString("#0"));
                        cmd3.Parameters.AddWithValue("@lifecycle_Average", avgLifecycle.ToString("#0"));
                        cmd3.Parameters.AddWithValue("@Failure_percent", failed_rate.ToString("#0.00") + " %");
                        cmd3.ExecuteNonQuery();
                        connection.Close();
                    }
                }
                // 關閉資料庫連線
                sqlconnection.Close();
                // 新增連線
                SqlConnection Conn = new SqlConnection();
                // 新稱SQL指令
                SqlCommand cmd1 = new SqlCommand();
                //SQL 資料庫的連接
                SqlDataAdapter da = new SqlDataAdapter();
                DataSet ds = new DataSet();
                DataTable dataTable = new DataTable();
                string str2 = Server.HtmlEncode(ddl_UUT_SN.SelectedItem.Value.ToString());


                // 設定要連線的資料庫及登入的帳密
                cmd1.Connection = Conn;
                Conn.ConnectionString = "Server=localhost;User id=LoginAIDC;Password=aidc2019;database=test;";
                // 選取report中的資料行
                //cmd1.CommandText = "Select Class as '種類', Components_number as '零件件號', lifecycle as '維修週期(天)(目前/平均)' ,lifecycle_Current as '維修週期(天)(目前)', lifecycle_Average as '維修週期(天)(平均)' FROM report";   //執行SQL語法進行查詢
                cmd1.CommandText = "Select Class as '系統件', Components_number as '零件件號', lifecycle_Current as '維修週期(天)(目前)', lifecycle_Average as '維修週期(天)(平均)' FROM report";   //執行SQL語法進行查詢
                                                                                                                                                                               // da選擇資料來源，由cmd1載入進來
                da.SelectCommand = cmd1;
                //da把資料填入ds裡面
                da.Fill(ds, "final");
                dataTable = ds.Tables["final"];

                // 將DataTable的資料載入到gv_ShowData內   
                gv_ShowData.DataSource = dataTable;
                // 將資料繫結到gv_ShowData內
                gv_ShowData.DataBind();
                // 顯示產生報表的按鈕
                GenerateReportBtn.Visible = true;
                lbl_Title_ShowData.Visible = true;
            }
        }

        // 將待測物的失效機率和生命週期匯入report資料表
        private void InsertUUTInformToDB(SqlConnection connection, string sUUT_SerialNum)
        {
            // 計算該待測物生命週期
            var UUT_LifeCycle = ComputeUUTLifecycle(connection, sUUT_SerialNum);
            // 取得該待測物上次出貨日期
            DateTime UUT_LastShipDate = new DateTime();
            UUT_LastShipDate = GetUUTLastShipDate(connection, sUUT_SerialNum, UUT_LastShipDate);
            // 計算目前使用時間
            TimeSpan usedTime = DateTime.Now - UUT_LastShipDate;
            // 計算該待測物的失效機率
            var UUT_FailedRate = (usedTime.Days / UUT_LifeCycle) * 100;
            // 計算該待測物所剩的時間
            var UUT_remainDays = UUT_LifeCycle - usedTime.Days;
            // 連接資料庫
            connection.Open();
            // SQL匯入report資料表的指令
            string cmdText = @"INSERT INTO report (BEC_Serial_number, Class, Components_number, lifecycle, lifecycle_Current, lifecycle_Average, Failure_percent) VALUES (@BEC_Serial_number, @Class, @Components_number, @lifecycle, @lifecycle_Current, @lifecycle_Average, @Failure_percent ); ";
            // 新增指令
            SqlCommand cmd = new SqlCommand(cmdText, connection);
            cmd.Parameters.AddWithValue("@BEC_Serial_number", sUUT_SerialNum);
            //cmd.Parameters.AddWithValue("@Class", "BEC");
            cmd.Parameters.AddWithValue("@Class", Session["WorkStation"].ToString());
            cmd.Parameters.AddWithValue("@Components_number", sUUT_SerialNum);
            cmd.Parameters.AddWithValue("@lifecycle", UUT_remainDays.ToString("#0") + "/" + UUT_LifeCycle.ToString("#0"));
            cmd.Parameters.AddWithValue("@lifecycle_Current", UUT_remainDays.ToString("#0"));
            cmd.Parameters.AddWithValue("@lifecycle_Average", UUT_LifeCycle.ToString("#0"));
            cmd.Parameters.AddWithValue("@Failure_percent", UUT_FailedRate.ToString("#0.00") + " %");
            // 執行指令
            cmd.ExecuteNonQuery();
            // 關閉資料庫連線
            connection.Close();
        }

        // 取得待測物上次出貨日期
        private DateTime GetUUTLastShipDate(SqlConnection connection, string sUUT_SerialNum, DateTime UUT_LastShipDate)
        {
            // 連接資料庫
            connection.Open();
            // SQL指令搜尋上次出貨日期
            String str = "SELECT MAX(Ship_Date) FROM final_new WHERE BEC_Serial_number = '" + sUUT_SerialNum + "'";
            // 新增指令
            SqlCommand cmd = new SqlCommand(str, connection);
            // 執行指令
            SqlDataReader reader = cmd.ExecuteReader();
            // 讀取指令執行結果
            while (reader.Read())
            {
                // 存取待測物上次出貨日期
                UUT_LastShipDate = DateTime.Parse(reader.GetString(0));
            }
            // 關閉資料庫連線
            connection.Close();
            // 回傳待測物上次出貨日期
            return UUT_LastShipDate;
        }

        // 清除Report資料表
        private void TruncateReportTable(SqlConnection connection)
        {
            connection.Open();
            string delcommand = "TRUNCATE TABLE report";
            SqlCommand delcmd = new SqlCommand(delcommand, connection);
            delcmd.ExecuteNonQuery();
            connection.Close();
        }

        // 取得生命週期和失效機率設定值
        private void GetDefaultSettings(SqlConnection connection, out int defaultLifecycle, out int defaultFailedRate)
        {
            // 開啟資料庫連線
            connection.Open();
            // 取得生命週期和失效機率設定值SQL指令
            string cmd = "SELECT lifecycle,Failure_percent FROM Settings";
            // 新增指令
            SqlCommand tempcmd = new SqlCommand(cmd, connection);
            // 執行指令
            SqlDataReader reader = tempcmd.ExecuteReader();
            defaultLifecycle = 0;
            defaultFailedRate = 0;
            while (reader.Read())
            {
                // 取得設定值
                defaultLifecycle = reader.GetInt32(0);
                defaultFailedRate = reader.GetInt32(1);
            }
            reader.Close();
            // 關閉資料庫連線
            connection.Close();
        }


        // 取得設定中的密碼
        private void GetPWD(SqlConnection connection)
        {
            // 開啟資料庫連線
            connection.Open();
            // SQL 撈取密碼的指令
            var pwdcommand = "SELECT password FROM Settings";
            // 新增資料庫命令
            SqlCommand pwdcmd = new SqlCommand(pwdcommand, connection);
            // 執行命令
            SqlDataReader pwdreader = pwdcmd.ExecuteReader();
            // 讀取密碼
            while (pwdreader.Read())
            {
                PWD.Text = pwdreader.GetString(0);
            }
            // 關閉資料庫連線
            connection.Close();
        }

        //// 取得所有BEC序號
        //private void GetAllBECSerialNum(SqlConnection connection)
        //{
        //    // 開啟資料庫連線
        //    connection.Open();
        //    // SQL 撈取密碼的指令
        //    String str = "SELECT DISTINCT BEC_Serial_number FROM final_new";
        //    // 新增資料庫命令
        //    SqlCommand cmd = new SqlCommand(str, connection);
        //    // 執行命令
        //    SqlDataReader reader = cmd.ExecuteReader();
        //    while (reader.Read())
        //    {
        //        // 存取所有BEC序號
        //        ddl_BEC_SN.Items.Add(reader.GetString(0));
        //    }
        //    // 關閉資料庫連線
        //    connection.Close();
        //}

        // 計算生命週期
        private float ComputeLifecycle(SqlConnection connection, string component)
        {
            // 開啟資料庫連線
            connection.Open();
            // SQL搜尋資料庫裡所有該零件資料的指令
            String cmdText = "SELECT *  FROM final_new WHERE Components_number='" + component + "'";
            // 新增資料庫命令
            SqlCommand cmd = new SqlCommand(cmdText, connection);
            // 執行命令
            SqlDataReader reader = cmd.ExecuteReader();
            TimeSpan temp;
            float num = 0, total = 0;
            while (reader.Read())
            {
                // 取得上次出貨日期跟接收日期計算使用時間並加總
                DateTime shipDate = DateTime.Parse(reader.GetString(3));
                DateTime receiveDate = DateTime.Parse(reader.GetString(4));
                temp = receiveDate - shipDate;
                total = total + temp.Days;
                num++;
            }
            // 計算平均使用時間
            var avgLifecycle = total / num;
            // 關閉資料庫連線
            connection.Close();
            // 回傳平均生命週期
            return avgLifecycle;
        }

        // 計算待測物生命週期
        private float ComputeUUTLifecycle(SqlConnection connection, string serialNum)
        {
            // 開啟資料庫連線
            connection.Open();
            // SQL搜尋資料庫裡所有該待測物資料的指令
            String cmdText = "SELECT *  FROM final_new WHERE BEC_Serial_number='" + serialNum + "'";
            // 新增資料庫命令
            SqlCommand cmd = new SqlCommand(cmdText, connection);
            // 執行命令
            SqlDataReader reader = cmd.ExecuteReader();
            TimeSpan temp;
            float num = 0, total = 0;
            while (reader.Read())
            {
                // 取得上次出貨日期跟接收日期計算使用時間並加總
                DateTime shipDate = DateTime.Parse(reader.GetString(3));
                DateTime receiveDate = DateTime.Parse(reader.GetString(4));
                temp = receiveDate - shipDate;
                total = total + temp.Days;
                num++;
            }
            // 計算平均使用時間
            var avgLifecycle = total / num;
            // 關閉資料庫連線
            connection.Close();
            // 回傳平均生命週期
            return avgLifecycle;
        }

        // 開啟資料庫連線
        private SqlConnection OpenSQLConnection()
        {
            // 設定要連線的資料庫及登入的帳密
            String conn = "Server=localhost;User id=LoginAIDC;Password=aidc2019;database=test;";
            // 建立連線
            SqlConnection connection = new SqlConnection(conn);
            // 回傳該連線
            return connection;
        }

        // 整理R程式的輸出
        private string ProcessROutput(string r_output)
        {
            // R程式輸出字串處理
            int indexOfLevels = r_output.IndexOf("Levels:");
            if (indexOfLevels >= 0)
            {
                r_output = r_output.Remove(indexOfLevels - 4);
            }
            return r_output.Replace("\r\n", "").Replace(" 1 ", "").Replace("  ", " ").Replace("  ", " ");
        }

        // 從R的輸出產生失效零件的list
        private List<string> GetComponent(string output)
        {
            // 創建component_list存放零件名稱
            List<string> component_list = new List<string>();
            // 做字串處理整理出零件名稱
            int indexOfComponent = output.IndexOf("[1]", output.IndexOf("[1]") + 1);
            int length = output.Length - indexOfComponent;
            var component_name = output.Substring(indexOfComponent, length);
            // 用正則表達式分割字串取得零件名稱
            string pattern = @"\[+\d+\]+";
            var component_name_elements = Regex.Split(component_name, pattern);
            for (int i = 1; i < component_name_elements.Length; i++)
            {
                component_list.AddRange(component_name_elements[i].Trim().Replace("  ", " ").Split(' '));
            }
            // 回傳零件名稱清單
            return component_list;
        }

        // R的輸出產生失效機率的list
        private List<float> GetFailedRate(string output)
        {
            // 創建failedrate_list存放零件失效機率
            List<float> failedrate_list = new List<float>();
            // 做字串處理整理出零件失效機率
            int indexOfComponent = output.IndexOf("[1]", output.IndexOf("[1]") + 1);
            var failed_rate = output.Substring(0, indexOfComponent);
            // 用正則表達式分割字串取得零件失效機率
            string pattern = @"\[+\d+\]+";
            var failed_rate_elements = Regex.Split(failed_rate, pattern);
            for (int i = 1; i < failed_rate_elements.Length; i++)
            {
                failedrate_list.Add(float.Parse(failed_rate_elements[i]) * 100);
            }
            // 回傳零件失效機率清單
            return failedrate_list;
        }

        // 執行Rscript執行檔
        private string RunRScript(string rpath, string scriptpath, string sUUT_SerialNum, int usedTime)
        {
            try
            {
                // 程序需要的資訊
                var info = new ProcessStartInfo
                {
                    FileName = rpath,
                    WorkingDirectory = Path.GetDirectoryName(scriptpath),
                    Arguments = scriptpath + " " + sUUT_SerialNum + " " + usedTime,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true,
                    UseShellExecute = false
                };
                // 新增程序以執行外部程式R
                using (var proc = new Process { StartInfo = info })
                {
                    // 開始程序
                    proc.Start();
                    // 回傳R程式output
                    return proc.StandardOutput.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return string.Empty;
        }

    }
}

