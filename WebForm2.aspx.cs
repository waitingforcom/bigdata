﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
using System.Linq;

namespace BigData
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        // 新增單筆維修紀錄載入頁面
        protected void Page_Load(object sender, EventArgs e)
        {
            // 如果使用者的登入狀態是空的，表示閒置太久或沒有登入
            if (Session["isLogin"] == null)
            {
                // 轉至登入頁面
                //Response.Redirect("Login.aspx");

                Response.Redirect("Info.aspx?info=3");
            }
            // 如果是第一次載入
            if (!IsPostBack)
            {
                //根據不同待測物變更控制項名稱
                string sUUT_Name = Session["WorkStation"].ToString();
                lbl_UUT_SN.Text = sUUT_Name + "序號：";
                lbl_UUT_PN.Text = sUUT_Name + "件號：";
                lbl_LastShipDate.Text = sUUT_Name + "上次出貨日期：";
                lbl_CompleteDate.Text = sUUT_Name + "完工日期：";

                GridView1.Columns[2].HeaderText = sUUT_Name + "序號";
                GridView1.Columns[3].HeaderText = sUUT_Name + "件號";
                GridView1.Columns[4].HeaderText = sUUT_Name + "完工日期";

                // 連接資料庫
                SqlConnection connection = OpenSQLConnection();
                // 取得設定中的密碼
                var pwdcommand = "SELECT password FROM Settings";
                // 新增SQL指令
                SqlCommand pwdcmd = new SqlCommand(pwdcommand, connection);
                // 執行SQL指令
                SqlDataReader pwdreader = pwdcmd.ExecuteReader();
                // 讀取執行結果
                while (pwdreader.Read())
                {
                    // 存取密碼
                    PWD.Text = pwdreader.GetString(0);
                }
                // 關閉資料庫連線
                connection.Close();

                // 開啟資料庫
                SqlConnection connection2 = OpenSQLConnection();
                // 清除SingleTempData的資料表(把新增單筆的清空)
                var command = "TRUNCATE TABLE SingleTempData;";
                // 新增SQL指令
                SqlCommand cmd = new SqlCommand(command, connection2);
                // 執行SQL指令
                cmd.ExecuteReader();
                // 關閉資料庫連線
                connection.Close();
            }
        }

        // 開啟資料庫連線
        private static SqlConnection OpenSQLConnection()
        {
            // 設定要連線的資料庫及登入的帳密
            String conn = "Server=localhost;User id=LoginAIDC;Password=aidc2019;database=test;";
            // 建立連線
            SqlConnection connection = new SqlConnection(conn);
            // 開啟連線
            connection.Open();
            // 回傳該連線
            return connection;
        }

        // 觸發新增的按鈕
        protected void Add_Click(object sender, EventArgs e)
        {
            SqlConnection connection = OpenSQLConnection();
            try
            {
                // 檢查工單號碼格式
                int workorderNum = Int32.Parse(WorkorderNum.Text);
                // 檢查零件數量格式
                int amount = Int32.Parse(Amount.Text);
                // 檢查所有日期格式
                DateTime dt4 = Convert.ToDateTime(LastShipDate.Text);
                DateTime dt5 = Convert.ToDateTime(ReceiveDate.Text);
                DateTime dt6 = Convert.ToDateTime(ShipDate.Text);
                // 開啟資料庫連線
                // 將單筆資料存入資料庫
                String cmdText = @" INSERT INTO SingleTempData 
                                    (Work_number, BEC_Serial_number, BEC_Part_Number, BEC_completion_Date, Receipt_Date, Ship_Date, Fault_Description, 
                                    CHECKLIST_Description, Detect_Failure_Record, Components_number, Components_name, number, counter) 
                                    VALUES 
                                    ( @Work_number, @BEC_Serial_number, @BEC_Part_Number, @BEC_completion_Date, @Receipt_Date, @Ship_Date, @Fault_Description,
                                    @CHECKLIST_Description, @Detect_Failure_Record, @Components_number, @Components_name, @number, @counter);";
                // 新增SQL命令
                SqlCommand cmd = new SqlCommand(cmdText, connection);
                cmd.Parameters.AddWithValue("@Work_number", WorkorderNum.Text);
                cmd.Parameters.AddWithValue("@BEC_Serial_number", SerialNum.Text);
                cmd.Parameters.AddWithValue("@BEC_Part_Number", PartNum.Text);
                cmd.Parameters.AddWithValue("@BEC_completion_Date", dt4.ToString("yyyy-MM-dd"));
                cmd.Parameters.AddWithValue("@Receipt_Date", dt5.ToString("yyyy-MM-dd"));
                cmd.Parameters.AddWithValue("@Ship_Date", dt6.ToString("yyyy-MM-dd"));
                cmd.Parameters.AddWithValue("@Fault_Description", Description.Text);
                cmd.Parameters.AddWithValue("@CHECKLIST_Description", Checklist.Text);
                cmd.Parameters.AddWithValue("@Detect_Failure_Record", FailedRecord.Text);
                cmd.Parameters.AddWithValue("@Components_number", ComponentNum.Text);
                cmd.Parameters.AddWithValue("@Components_name", ChineseName.Text);
                cmd.Parameters.AddWithValue("@number", Amount.Text);
                cmd.Parameters.AddWithValue("@counter", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                // 執行SQL命令
                cmd.ExecuteNonQuery();
                // 關閉資料庫連線
                connection.Close();
                // 清空資料欄位
                //ComponentNum.Text = "";
                //ChineseName.Text = "";
                //Amount.Text = "";
                // 綁定GridView1的資料來源
                GridView1.DataSourceID = "SqlDataSource1";
                // 顯示送出的按鈕
                SendBtn.Visible = true;
            }
            catch
            {
                // 若出錯，顯示錯誤訊息
                AddError.Text = "錯誤!! 新增失敗 請檢查資料是否正確";
                AddError.Visible = true;

                connection.Close();
                return;
            }
        }

        // 當使用者變更待測物序號
        protected void SerialNum_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string[] stringArray = new string[100];
                string[] stringArraya = new string[100];
                int i = 0, j = 0;
                // 設定要連線的資料庫及登入的帳密
                String conString = "Server=localhost;User id=LoginAIDC;Password=aidc2019;database=test;";
                // 建立連線
                SqlConnection connection = new SqlConnection(conString);
                // 開啟連線
                connection.Open();
                // 搜尋該待測物序號的完工日期與上次出貨日期的SQL指令
                var str = "SELECT BEC_completion_Date, Ship_Date FROM final_new where BEC_Serial_number LIKE '" + SerialNum.Text + "'";
                // 新增指令
                SqlCommand cmd = new SqlCommand(str, connection);
                // 執行指令
                SqlDataReader reader = cmd.ExecuteReader();
                // 讀取完工日期與出貨日期
                while (reader.Read())
                {
                    stringArray[i] = reader.GetString(0);
                    i++;
                    stringArraya[j] = reader.GetString(1);
                    j++;
                }
                // 存取完工日期與出貨日期
                DateTime parsedDate = DateTime.Parse(stringArray[i - 1]);
                DateTime shipDate = DateTime.Parse(stringArray[j - 1]);
                CompleteDate.Text = parsedDate.ToString("yyyy-MM-dd");
                LastShipDate.Text = shipDate.ToString("yyyy-MM-dd");
                // 關閉資料庫連線
                connection.Close();
            }
            catch
            {
                // 若出錯，顯示錯誤訊息
                //string msg = "查無上次出貨日期與完工日期 請確定待測物序號是否正確";

                //直接不顯示提示訊息
                //string sUUT_Name = Session["WorkStation"].ToString();
                //string msg = "查無上次出貨日期與完工日期 請確定" + sUUT_Name + "序號是否正確";
                //string Alertmsg = "<script language = JavaScript > alert('" + msg + "'); </script>";
                //Response.Write(Alertmsg);

                //清空控制項
                CompleteDate.Text = "";
                LastShipDate.Text = "";

            }
        }

        // 觸發送出按鈕
        protected void SendBtn_Click(object sender, EventArgs e)
        {
            try
            {
                int temp = 0;
                // 開啟資料庫連線
                SqlConnection connection = OpenSQLConnection();
                // 搜尋數量SQL語法
                String cmdText1 = "SELECT DISTINCT counter FROM final_new";
                // 新增SQL指令
                SqlCommand cmd1 = new SqlCommand(cmdText1, connection);
                // 執行SQL指令
                SqlDataReader reader1 = cmd1.ExecuteReader();
                // 讀取執行後的值
                while (reader1.Read())
                {
                    temp = reader1.GetInt16(0);
                }
                // 關閉資料庫連線
                connection.Close();

                // 開啟資料庫連線
                connection.Open();
                // 選取所有要新增的單筆紀錄
                String cmdText2 = "SELECT * FROM SingleTempData";
                // 新增指令
                SqlCommand cmd2 = new SqlCommand(cmdText2, connection);
                // 執行指令
                SqlDataReader reader2 = cmd2.ExecuteReader();
                // 讀取選取的結果
                while (reader2.Read())
                {
                    // 開啟新的資料庫連線
                    SqlConnection connection2 = OpenSQLConnection();
                    // 將資料輸入資料庫SQL指令
                    String cmdText = " INSERT INTO final_new (Work_number, BEC_Serial_number, BEC_Part_Number, BEC_completion_Date, Receipt_Date, Ship_Date, Fault_Description, CHECKLIST_Description, Detect_Failure_Record, Components_number, Components_name, number, counter) VALUES ( '" + reader2.GetInt32(0) + "', '" + reader2.GetString(1) + "', '" + reader2.GetString(2) + "', '" + reader2.GetString(3) + "', '" + reader2.GetString(4) + "', '" + reader2.GetString(5) + "', N'" + reader2.GetString(6) + "', N'" + reader2.GetString(7) + "', N'" + reader2.GetString(8) + "', '" + reader2.GetString(9) + "', N'" + reader2.GetString(10) + "', '" + reader2.GetInt32(11) + "', '" + (temp + 1) + "');";
                    // 新增指令
                    SqlCommand cmd = new SqlCommand(cmdText, connection2);
                    // 執行指令
                    cmd.ExecuteNonQuery();
                    // 關閉資料庫連線
                    connection2.Close();
                    // 將數量加一以新增下一筆資料
                    temp = temp + 1;
                }
                // 關閉資料庫連線
                connection.Close();
                // 顯示送出成功的訊息
                string msg = "送出成功";
                string Alertmsg = "<script language = JavaScript > alert('" + msg + "'); </script>";
                Response.Write(Alertmsg);
                // 清空資料欄位
                ClearData();

                // 開啟資料庫連線
                SqlConnection connection3 = OpenSQLConnection();
                // SQL清空SingleTempData資料表指令
                var command = "TRUNCATE TABLE SingleTempData;";
                // 新增指令
                SqlCommand cmd3 = new SqlCommand(command, connection3);
                // 執行指令
                cmd3.ExecuteReader();
                // 關閉資料庫連線
                connection.Close();

            }
            catch
            {
                // 若出錯，顯示錯誤訊息
                string msg = " 錯誤!! 送出失敗 請檢查資料是否正確 ";
                string Alertmsg = "<script language = JavaScript > alert('" + msg + "'); </script>";
                Response.Write(Alertmsg);
            }
        }

        // 清空所有欄位
        private void ClearData()
        {
            WorkorderNum.Text = "";
            SerialNum.Text = "";
            PartNum.Text = "";
            LastShipDate.Text = "";
            CompleteDate.Text = "";
            ReceiveDate.Text = "";
            ShipDate.Text = "";
            Description.Text = "";
            Checklist.Text = "";
            FailedRecord.Text = "";
            ComponentNum.Text = "";
            ChineseName.Text = "";
            Amount.Text = "";
            GridView1.Columns.Clear();
            SendBtn.Visible = false;
        }
    }
}