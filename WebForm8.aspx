﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm8.aspx.cs" Inherits="BigData.WebForm8" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title></title>
    <style type="text/css">
        .auto-style1 {
            text-align: center;
            font-family: DFKai-sb,"標楷體";
        }

        .auto-style2 {
            font-size: xx-large;
            font-family: DFKai-sb,"標楷體";
        }

        .auto-style3 {
            text-align: left;
            font-size: x-large;
            font-family: DFKai-sb,"標楷體";
        }

        .GridViewEditRow input[type=text] {
            width: 100px;
        }
    </style>
    <link href="layout.css" rel="stylesheet" type="text/css" />
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="main.js"></script>
</head>
<script>
    $(document).ready(function () {
        $(function () {
            var pwd = localStorage["AdminPWD"];
            if (pwd === "AIDC2019") {
                var userManagement = document.getElementById("UserManagement");
                userManagement.style.display = 'block';
            }
        });
    });
</script>
<body>
    <form id="form1" runat="server">
        <div class="container-fluid img-fluid">
            <asp:Image ID="img_Title" runat="server" ImageUrl="~/title1.png" />
            <p hidden>
                <asp:Label ID="PWD" runat="server"></asp:Label>
            </p>
        </div>

        <div id="NAV" class="container-fluid">
            <ul class="navbar-nav nav-fill w-100 d-inline-block">
                <li class="w-25"><a href="WebForm4.aspx">零件診斷</a></li>
                <li class="w-25" onclick="toggle()">
                    <a>新增維修紀錄 <span class="caret"></span></a>
                    <ul id="Dropdownlist" style="display: none;">
                        <li><a href="WebForm2.aspx">新增單筆維修紀錄</a></li>
                        <li><a href="WebForm3.aspx">新增多筆維修紀錄</a></li>
                    </ul>
                </li>
                <li class="w-25"><a href="WebForm9.aspx">資料庫功能</a></li>
                <%--<li><a href="WebForm1.aspx"><font size="6">歷史分析</font></a></li>--%>
                <li class="w-25"><a class="active" href="WebForm8.aspx">管理頁面</a></li>
            </ul>
        </div>
        <div align="center">
            <h2 class="auto-style1 auto-style2" style="color: aliceblue">零件診斷</h2>
            <div class="well well-lg auto-style1 auto-style2" style="width: 700px; background-color: aliceblue">
                零件更換提醒&nbsp;&lt;&nbsp;&nbsp;<asp:TextBox ID="RemindTime" runat="server" Width="80px" Height="40px"></asp:TextBox>&nbsp;&nbsp;天
                <p></p>
                零件失效機率&nbsp;&gt;&nbsp;&nbsp;<asp:TextBox ID="FailedRate" runat="server" Width="80px" Height="40px"></asp:TextBox>&nbsp;&nbsp;%&nbsp;
                <p></p>
                <asp:TextBox ID="BECErrorMsg" runat="server" BorderStyle="None" Style="background-color: transparent" ForeColor="#CC3300" Visible="False" Width="535px"></asp:TextBox>
                <p></p>
                <asp:Button ID="BECConfirmBtn" runat="server" OnClick="BECConfirmBtn_Click" Text="確定" Height="50px" Width="93px" />
            </div>
            <h2 class="auto-style1 auto-style2" style="color: aliceblue">資料庫功能</h2>
            <div class="well well-lg auto-style1 auto-style2" style="width: 700px; background-color: aliceblue">
                <font color="#1B3563" size="6px" class="auto-style2">歷史資料備份: 從</font>
                <asp:TextBox ID="StartYear" runat="server" Width="80px" Height="40px"></asp:TextBox>
                <font color="#1B3563" size="6px" class="auto-style2">年 ~ </font>
                <asp:TextBox ID="EndYear" runat="server" Width="80px" Height="40px"></asp:TextBox>
                <font color="#1B3563" size="6px" class="auto-style2">年</font>
                <p></p>
                <asp:TextBox ID="DBErrorMsg" runat="server" BorderStyle="None" Style="background-color: transparent" ForeColor="#CC3300" Visible="False" Width="535px"></asp:TextBox>
                <p></p>
                <asp:Button ID="HistoryConfirmBtn" runat="server" OnClick="HistoryConfirmBtn_Click" Text="確定" Height="50px" Width="93px" />
            </div>

            <h2 class="auto-style1 auto-style2" style="color: aliceblue">歷史分析</h2>
            <div class="well well-lg auto-style1 auto-style2" style="width: 700px; background-color: aliceblue">
                歷史資料分析 
                <asp:TextBox ID="HistoryYear" runat="server" Width="50px" Height="40px"></asp:TextBox>
                &nbsp;年&nbsp;&nbsp; 
                <p></p>
                <asp:TextBox ID="HistoryErrorMsg" runat="server" BorderStyle="None" Style="background-color: transparent" ForeColor="#CC3300" Visible="False" Width="535px"></asp:TextBox>
                <p></p>
                <asp:Button ID="HistoryBtn" runat="server" OnClick="HistoryBtn_Click" Text="確定" Height="50px" Width="93px" />
            </div>
            <h2 class="auto-style1 auto-style2" style="color: aliceblue">管理頁面密碼修改</h2>
            <div class="well well-lg auto-style1 auto-style2" style="width: 700px; background-color: aliceblue">
                密碼&nbsp;&nbsp;<asp:TextBox ID="Password" runat="server" Width="300px" Height="40px"></asp:TextBox>
                <p></p>
                <asp:Button ID="pwdButton" runat="server" OnClick="PwdButton_Click" Text="確定" Height="50px" Width="93px" />
            </div>
            <div id="UserManagement" style="display: none;">
                <h2 class="auto-style1 auto-style2" style="color: aliceblue">新增使用者</h2>
                <div class="well well-lg auto-style1 auto-style2" style="width: 900px; background-color: aliceblue">
                    帳號&nbsp;&nbsp;<asp:TextBox ID="NewUserAccount" runat="server" Width="300px" Height="40px"></asp:TextBox>
                    <p></p>
                    密碼&nbsp;&nbsp;<asp:TextBox ID="NewUserPassword" runat="server" Width="300px" Height="40px"></asp:TextBox>
                    <p></p>
                    使用者名稱&nbsp;&nbsp;<asp:TextBox ID="NewUserName" runat="server" Width="300px" Height="40px"></asp:TextBox>
                    <p></p>
                    <div align="center">
                        <asp:RadioButtonList ID="UserList" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Value="1">&nbsp;&nbsp;管理者&nbsp;&nbsp;</asp:ListItem>
                            <asp:ListItem Value="0">&nbsp;&nbsp;一般使用者&nbsp;&nbsp;</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <asp:Button ID="Button1" runat="server" OnClick="AddNewUser_Click" Text="確定" Height="50px" Width="93px" />
                    <asp:Button ID="EditUserBtn" runat="server" OnClick="EditUser_Click" Text="使用者管理" Height="50px" Width="180px" />
                    <p></p>
                    <p></p>
                    <asp:GridView ID="GridView1" runat="server" Width="850px" AutoGenerateColumns="False" DataKeyNames="Account" DataSourceID="SqlDataSource1" Visible="false">
                        <EditRowStyle BackColor="PaleGoldenRod " CssClass="GridViewEditRow" />
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:TemplateField ShowHeader="False">
                                <EditItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="更新" OnClientClick="return confirm('確認要修改嗎？');"></asp:LinkButton>
                                    &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="取消"></asp:LinkButton>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit" Text="編輯"></asp:LinkButton>
                                    &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Delete" Text="刪除" OnClientClick="return confirm('確認要刪除嗎？');"></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="200px" HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="Account" HeaderText="帳號" SortExpression="Account">
                                <ItemStyle Width="100px" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Password" HeaderText="密碼" SortExpression="Password">
                                <ItemStyle Width="20px" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <%--  <asp:TemplateField HeaderText="密碼">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Text='<%# Bind("Password") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="TextBox2" Enabled="false" runat="server" TextMode="Password" Text='<%# Bind("Password") %>'></asp:TextBox>
                                </ItemTemplate>
                                <ItemStyle Width="100px" HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="Username" HeaderText="名稱" SortExpression="Username">
                                <ItemStyle Width="100px" HorizontalAlign="Left" />
                            </asp:BoundField>--%>
                            <%--<asp:BoundField DataField="IsAdmin" HeaderText="管理者" SortExpression="IsAdmin">
                            <ItemStyle Width="10px" />
                        </asp:BoundField>--%>
                            <asp:CheckBoxField
                                DataField="IsAdmin"
                                HeaderText="管理者"
                                InsertVisible="True"
                                ShowHeader="True"
                                SortExpression="IsAdmin"
                                Visible="True">
                                <ItemStyle Width="80px" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:CheckBoxField>
                        </Columns>
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" DeleteCommand="DELETE FROM [UserInform] WHERE [Account] = @original_Account AND [Password] = @original_Password AND [Username] = @original_Username AND [IsAdmin] = @original_IsAdmin"
                        InsertCommand="INSERT INTO [UserInform] ([Account], [Password], [Username], [IsAdmin]) VALUES (@Account, @Password, @Username, @IsAdmin)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [UserInform]"
                        UpdateCommand="UPDATE [UserInform] SET [Account] = @Account, [Password] = @Password, [Username] = @Username, [IsAdmin] = @IsAdmin WHERE [Account] = @original_Account AND [Password] = @original_Password AND [Username] = @original_Username AND [IsAdmin] = @original_IsAdmin">
                        <DeleteParameters>
                            <asp:Parameter Name="original_Account" Type="String" />
                            <asp:Parameter Name="original_Password" Type="String" />
                            <asp:Parameter Name="original_Username" Type="String" />
                            <asp:Parameter Name="original_IsAdmin" Type="String" />
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="Account" Type="String" />
                            <asp:Parameter Name="Password" Type="String" />
                            <asp:Parameter Name="Username" Type="String" />
                            <asp:Parameter Name="IsAdmin" Type="String" />
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="Account" Type="String" />
                            <asp:Parameter Name="Password" Type="String" />
                            <asp:Parameter Name="Username" Type="String" />
                            <asp:Parameter Name="IsAdmin" Type="String" />
                            <asp:Parameter Name="original_Account" Type="String" />
                            <asp:Parameter Name="original_Password" Type="String" />
                            <asp:Parameter Name="original_Username" Type="String" />
                            <asp:Parameter Name="original_IsAdmin" Type="String" />
                        </UpdateParameters>
                    </asp:SqlDataSource>
                </div>
            </div>
        </div>
        <br />
    </form>
</body>
</html>
