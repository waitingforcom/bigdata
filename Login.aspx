﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="BigData.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>零件維修大數據統計系統</title>
    <style type="text/css">
        .auto-style1 {
            text-align: center;
            font-size: 28px;
            font-family: DFKai-sb,"標楷體";
        }

        .auto-style2 {
            font-size: 28px;
            font-family: DFKai-sb,"標楷體";
        }

        .auto-style3 {
            font-size: 34px;
            font-family: DFKai-sb,"標楷體";
        }

        .auto-style4 {
            text-align: left;
            font-size: 34px;
            font-family: DFKai-sb,"標楷體";
        }
    </style>
    <link href="layout.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<script type="text/javascript" src="main.js"></script>
<link href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.min.css"
    rel="stylesheet" type="text/css" />
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.min.js"></script>

<script>
    function ShowPopup() {
        $(function () {
            $(".ui-dialog").show();
            $("#popup2").dialog({
                height: 400,
                width: 550
            });
        });
    };
    function HidePopup() {
        $(function () {
            $("#popup2").hide();
            $(".ui-dialog").hide();
            ClearTextBox();
        });
    };
    function ClearTextBox() {
        $(function () {
            $("#AccountID").val("");
            $("#OriginPWD").val("");
            $("#NewPWD").val("");
            $("#ConfirmNewPWD").val("");
        });
    };
    function Check() {
        $(function () {
            $("#HiddenAccount").val($("#AccountID").val()); 
            $("#HiddenOriginPWD").val($("#OriginPWD").val()); 
            $("#HiddenNewPWD").val($("#NewPWD").val()); 
            $("#HiddenConfirmNewPWD").val($("#ConfirmNewPWD").val()); 
        });
    };
</script>
<body>
    <form id="form2" runat="server">
        <div class="container-fluid img-fluid">
            <asp:Image ID="img_Title" runat="server" ImageUrl="~/title1.png" />
            <p hidden>
                <asp:Label ID="PWD" runat="server"></asp:Label>
            </p>
        </div>
        <asp:HiddenField ID="HiddenAccount" runat="server" OnValueChanged="SetAccountID"/>
        <asp:HiddenField ID="HiddenOriginPWD" runat="server" OnValueChanged="SetOriginPWD"/>
        <asp:HiddenField ID="HiddenNewPWD" runat="server" OnValueChanged="SetNewPWD"/>
        <asp:HiddenField ID="HiddenConfirmNewPWD" runat="server" OnValueChanged="SetConfirmNewPWD"/>
        <div id="popup2" class="popup_block" style="background-color: #FFF; display: none;">
            <div class="inner_content2-2 auto-style2" style="width:400px;height:200px">
                <h1>更換密碼</h1>
                <p></p>
                帳號&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="AccountID" runat="server" Width="200px" Height="40px"></asp:TextBox>
                <p></p>
                原密碼&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="OriginPWD" runat="server" Width="200px" Height="40px" TextMode="Password"></asp:TextBox>
                <p></p>
                新密碼&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="NewPWD" runat="server" Width="200px" Height="40px" TextMode="Password"></asp:TextBox>
                <p></p>
                確認新密碼&nbsp;&nbsp;&nbsp;<asp:TextBox ID="ConfirmNewPWD" runat="server" Width="200px" Height="40px" TextMode="Password"></asp:TextBox>
                <p></p><p></p><p></p>
                <div align="center" style="margin: 0 40px">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="Confirm" runat="server" Text="確認" Height="50px" Width="93px" OnClick="ChangePWD_Click" UseSubmitBehavior="false" OnClientClick="Check();"/>
                    <asp:Button ID="Cancel" runat="server" Text="取消" Height="50px" Width="93px" OnClientClick="javascript:HidePopup();return false;" />
                </div>
            </div>
        </div>
        <div align="center">
            <h2 class="auto-style1" style="color: aliceblue; font-size: 44px;">系統登入</h2>
            <div class="well well-lg auto-style1" style="width: 600px; background-color: aliceblue">
                帳號&nbsp;&nbsp;&nbsp;<asp:TextBox ID="Account" runat="server" Width="300px" Height="40px"></asp:TextBox>&nbsp;&nbsp;
                <p></p>
                密碼&nbsp;&nbsp;&nbsp;<asp:TextBox ID="Password" runat="server" Width="300px" Height="40px" TextMode="Password"></asp:TextBox>&nbsp;&nbsp;
                <p></p>
                <asp:Button ID="LoginBtn" runat="server" Text="登入" Height="50px" Width="93px" OnClick="Login_Click" />
                <asp:LinkButton ID="ChangePWDLink" runat="server" Text="修改密碼" Height="50px" Width="120px" OnClientClick="javascript:ShowPopup();return false;" />
            </div>
        </div>
    </form>
</body>
</html>
