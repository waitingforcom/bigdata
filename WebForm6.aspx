﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm6.aspx.cs" Inherits="BigData.WebForm6" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=15.0.0.0, Culture=neutral, PublicKeyToken=89845DCD8080CC91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>零件維修大數據統計系統</title>
    <style type="text/css">
        .auto-style1 {
            text-align: center;
        }

        .auto-style2 {
            text-align: left;
        }
    </style>
    <link href="layout.css" rel="stylesheet" type="text/css" />
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
</head>
<script type="text/javascript" src="main.js"></script>
<body>
    <form id="form1" runat="server">
        <div>

            <div class="container-fluid img-fluid">
                <asp:Image ID="img_Title" runat="server" ImageUrl="~/title1.png" />
                <p hidden>
                    <asp:Label ID="PWD" runat="server"></asp:Label>
                </p>
            </div>

            <div id="NAV" class="container-fluid">
                <ul class="navbar-nav nav-fill w-100 d-inline-block">
                    <li class="w-25"><a href="WebForm4.aspx">零件診斷</a></li>
                    <li class="w-25" onclick="toggle()">
                        <a>新增維修紀錄 <span class="caret"></span></a>
                        <ul id="Dropdownlist" style="display: none;">
                            <li><a href="WebForm2.aspx">新增單筆維修紀錄</a></li>
                            <li><a href="WebForm3.aspx">新增多筆維修紀錄</a></li>
                        </ul>
                    </li>
                    <li class="w-25"><a href="WebForm9.aspx">資料庫功能</a></li>
                    <%--<li><a href="WebForm1.aspx"><font size="6">歷史分析</font></a></li>--%>
                    <li class="w-25"><a onclick="inputPassword()">管理頁面</a></li>
                </ul>
            </div>

            <div id="CONTENT">
                <asp:ScriptManager runat="server"></asp:ScriptManager>
                <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%" Height="1352px" BackColor="" ClientIDMode="AutoID"
                    HighlightBackgroundColor="" InternalBorderColor="204, 204, 204" InternalBorderStyle="Solid" InternalBorderWidth="1px"
                    LinkActiveColor="" LinkActiveHoverColor="" LinkDisabledColor="" PrimaryButtonBackgroundColor="" PrimaryButtonForegroundColor=""
                    PrimaryButtonHoverBackgroundColor="" PrimaryButtonHoverForegroundColor="" SecondaryButtonBackgroundColor="" SecondaryButtonForegroundColor=""
                    SecondaryButtonHoverBackgroundColor="" SecondaryButtonHoverForegroundColor="" SplitterBackColor="" ToolbarDividerColor="" ToolbarForegroundColor=""
                    ToolbarForegroundDisabledColor="" ToolbarHoverBackgroundColor="" ToolbarHoverForegroundColor="" ToolBarItemBorderColor="" ToolBarItemBorderStyle="Solid"
                    ToolBarItemBorderWidth="1px" ToolBarItemHoverBackColor="" ToolBarItemPressedBorderColor="51, 102, 153" ToolBarItemPressedBorderStyle="Solid"
                    ToolBarItemPressedBorderWidth="1px" ToolBarItemPressedHoverBackColor="153, 187, 226" DocumentMapCollapsed="True" DocumentMapWidth="100%"
                    PromptAreaCollapsed="True" ZoomMode="PageWidth">
                    <LocalReport ReportPath="Report2.rdlc">
                        <DataSources>
                            <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet_BigData" />
                        </DataSources>
                    </LocalReport>
                </rsweb:ReportViewer>
                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData" TypeName="BigData.DataSetTableAdapters.reportTableAdapter"></asp:ObjectDataSource>
            </div>
        </div>
    </form>
</body>
</html>
